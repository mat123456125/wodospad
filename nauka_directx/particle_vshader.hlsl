#include "particle_cbuffer.hlsl"





struct Vertex
{
	float3 PosW			: POSITION;
	float3 VelW			: VELOCITY;
	float2 SizeW		: SIZE;
	float Age			: AGE;
	uint Type			: TYPE;
	uint Texture		: TEX; // indeks w tablicy tekstur
	uint Rotation		: ROTATION;  //obroty o wielokrotnosc 90 stopni
};

struct VertexOut
{
	float3 PosW		: POSITION;
	float2 SizeW	: SIZE;
	float4 Color	: COLOR;
	uint Type		: TYPE;
	uint Texture	: TEX; 
	uint Rotation	: ROTATION;
};

VertexOut main(Vertex vin)
{
	
	VertexOut vout;
	float t = vin.Age;
	
	vout.PosW = vin.PosW;
	// fade color with time
	float maxtime = 60.0f;
	if(vin.Type == 2)
		maxtime = 1.2f;
	else if (vin.Type == 3)
		maxtime = 2.0f;
	float opacity = smoothstep(0.0f, 0.5f, t / 1.0f);
	opacity = opacity - smoothstep(0.7f, maxtime, t / 1.0f);//0.1f; 
	
	vout.Color = float4(0.9f,0.9f, 1.0f, opacity);
	vout.SizeW = vin.SizeW;
	vout.Type = vin.Type;
	vout.Texture = vin.Texture;
	vout.Rotation = vin.Rotation;
	return vout;
}