cbuffer cbPerFrame : register(b0)
{
	float3 gEyePosW;
	float pad;
	float3 gInitialVel;
	float pad1;
	float3 gAccel;
	float pad2;
	float gGameTime;
	float gTimeStep;
	float2 pad3;
	float4x4 gViewProj;
	float4 collPlane;  //do obliczania kolizji
};
