#pragma once

#include "directxDevice.h"
#include "programData.h"
#include "VertexTypes.h"
#include "light.h"
#include "meshMaterial.h"
#include <vector>

class mesh
{
public:
	
	mesh(int sID,int len, programData* pd, meshMaterial* mat);
	~mesh();

	void draw();

	objectLighting m_lighting; //ustawienie oswietlenia obietku
	meshMaterial* m_material;
	DirectX::SimpleMath::Matrix worldMatrix;
	int startIndex;
	int size;
	programData* prData;
	DirectxDevice* gDevice;
};

