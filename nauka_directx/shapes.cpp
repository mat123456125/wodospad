#include "shapes.h"

//skala textury dla wody
#define TEX_SCALE 1.0f
#define FALL_TEX_SCALE 1.0f

using namespace DirectX;
using namespace SimpleMath;
using namespace std;

shapes::triangle::triangle()
{
}

shapes::triangle::triangle(DirectX::SimpleMath::Vector3 a, DirectX::SimpleMath::Vector3 b, DirectX::SimpleMath::Vector3 c)
{
	vertex[0] = a;
	vertex[1] = b;
	vertex[2] = c;
}

void shapes::triangle::setNormal()
{
	Vector3 temp1, temp2; //do obliczania wektora normalnego
	temp1 = vertex[1] - vertex[0];
	temp2 = vertex[2] - vertex[0];
	temp1 = temp2.Cross(temp1);
	temp1.Normalize();
	triNormal = temp1;
}

void shapes::triangle::setTangent()
{
	Matrix E,UV;
	Vector2 uv0 = tex[1] - tex[0];
	Vector2 uv1 = tex[2] - tex[0];
	Vector3 e0 = vertex[1] - vertex[0];
	Vector3 e1 = vertex[2] - vertex[0];
	E = Matrix(e0, e1, Vector3::Zero);
	UV = Matrix(Vector3(uv1.y, -uv0.y, 0),
				Vector3(-uv1.x, uv0.x, 0),
				Vector3::Zero);
	
	Matrix A = (1/(uv0.x*uv1.y - uv0.y*uv1.x))* UV * E;
	triTangent = Vector3(A._11, A._12, A._13);
}

std::vector<VertexPositionColor> shapes::sphereVPC(int steps, Vector4 color)
{	
	vector<triangle> triangles = basicSphere();


	for (int i = 0; i < steps; i++)
	{
		triangles = refine(triangles);
	}
	

	
	vector<VertexPositionColor> ret_vertex;
	triangles.reserve(3*triangles.size());
	for (int i = 0; i < triangles.size(); i++)
	{
		ret_vertex.push_back(VertexPositionColor(triangles[i].vertex[0], color));
		ret_vertex.push_back(VertexPositionColor(triangles[i].vertex[1], XMFLOAT4(1, 0, 0, 1)));
		ret_vertex.push_back(VertexPositionColor(triangles[i].vertex[2], XMFLOAT4(0, 0, 1, 1)));
	}
	
	return ret_vertex;
}

std::vector<DirectX::VertexPositionNormalColor> shapes::sphereVPNC(int steps, DirectX::SimpleMath::Vector4 color)
{
	vector<triangle> triangles = basicSphere();


	for (int i = 0; i < steps; i++)
	{
		triangles = refine(triangles);
	}



	vector<VertexPositionNormalColor> ret_vertex;
	
	ret_vertex.reserve(3 * triangles.size());
	for (int i = 0; i < triangles.size(); i++)
	{
		
		ret_vertex.push_back(VertexPositionNormalColor(triangles[i].vertex[0], triangles[i].vertex[0], color));
		ret_vertex.push_back(VertexPositionNormalColor(triangles[i].vertex[1], triangles[i].vertex[1], XMFLOAT4(1, 0, 0, 1)));
		ret_vertex.push_back(VertexPositionNormalColor(triangles[i].vertex[2], triangles[i].vertex[2], XMFLOAT4(0, 0, 1, 1)));
	}

	return ret_vertex;
}

std::vector<DirectX::VertexPositionNormalTexture> shapes::sphereVPNT(int steps)
{
	vector<triangle> triangles = basicSphere();


	for (int i = 0; i < steps; i++)
	{
		triangles = refine(triangles);
	}



	vector<VertexPositionNormalTexture> ret_vertex;
	float u[3], v;
	ret_vertex.reserve(3 * triangles.size());
	for (int i = 0; i < triangles.size(); i++)
	{
		u[0] = 0.5f + atan2(triangles[i].vertex[0].z, triangles[i].vertex[0].x) / (2.0f*3.1416f);
		v = 0.5f - asin(triangles[i].vertex[0].y) / 3.1416f;
		ret_vertex.push_back(VertexPositionNormalTexture(triangles[i].vertex[0], triangles[i].vertex[0], Vector2(u[0], v)));
		u[1] = 0.5f + atan2(triangles[i].vertex[1].z, triangles[i].vertex[1].x) / (2.0f*3.1416f);
		v = 0.5f - asin(triangles[i].vertex[1].y) / 3.1416f;

		if (u[1] - u[0]>0.5f)
		{
			u[1] = u[1] - 1.0f;
		}
		if (u[1] - u[0]<-0.5f)
		{
			u[1] = u[1] + 1.0f;
		}

		ret_vertex.push_back(VertexPositionNormalTexture(triangles[i].vertex[1], triangles[i].vertex[1], Vector2(u[1], v)));
		u[2] = 0.5f + atan2(triangles[i].vertex[2].z, triangles[i].vertex[2].x) / (2.0f*3.1416f);
		v = 0.5f - asin(triangles[i].vertex[2].y) / 3.1416f;

		if (u[2] - u[1]>0.5f)
		{
			u[2] = u[2] - 1.0f;
		}
		if (u[2] - u[1]<-0.5f)
		{
			u[2] = u[2] + 1.0f;
		}
		ret_vertex.push_back(VertexPositionNormalTexture(triangles[i].vertex[2], triangles[i].vertex[2], Vector2(u[2], v)));
	}

	return ret_vertex;
}

std::vector<DirectX::VertexPositionNormalColor> shapes::rectangleVPNC(DirectX::SimpleMath::Vector2 size, DirectX::SimpleMath::Vector4 color)
{
	vector<VertexPositionNormalColor> ret_vertex;
	ret_vertex.push_back(VertexPositionNormalColor(Vector3(0, 0, 0), Vector3(0, 0, 1), color));
	ret_vertex.push_back(VertexPositionNormalColor(Vector3(0,size.y, 0), Vector3(0, 0, 1), color));
	ret_vertex.push_back(VertexPositionNormalColor(Vector3(size.x, size.y, 0), Vector3(0, 0, 1), color));
	ret_vertex.push_back(VertexPositionNormalColor(Vector3(0, 0, 0), Vector3(0, 0, 1), color));
	ret_vertex.push_back(VertexPositionNormalColor(Vector3(size.x, size.y, 0), Vector3(0, 0, 1), color));
	ret_vertex.push_back(VertexPositionNormalColor(Vector3(size.x, 0, 0), Vector3(0, 0, 1), color));


	return ret_vertex;
}

std::vector<DirectX::VertexPositionNormalTexture> shapes::rectangleVPNT(DirectX::SimpleMath::Vector2 size)
{
	vector<VertexPositionNormalTexture> ret_vertex;
	ret_vertex.push_back(VertexPositionNormalTexture(Vector3(0, 0, 0), Vector3(0, 0, 1), Vector2(0,1)));
	ret_vertex.push_back(VertexPositionNormalTexture(Vector3(0, size.y, 0), Vector3(0, 0, 1), Vector2(0, 0)));
	ret_vertex.push_back(VertexPositionNormalTexture(Vector3(size.x, size.y, 0), Vector3(0, 0, 1), Vector2(1, 0)));
	ret_vertex.push_back(VertexPositionNormalTexture(Vector3(0, 0, 0), Vector3(0, 0, 1), Vector2(0, 1)));
	ret_vertex.push_back(VertexPositionNormalTexture(Vector3(size.x, size.y, 0), Vector3(0, 0, 1), Vector2(1, 0)));
	ret_vertex.push_back(VertexPositionNormalTexture(Vector3(size.x, 0, 0), Vector3(0, 0, 1), Vector2(1, 1)));


	return ret_vertex;
}

std::vector<VertexPositionNormalTextureTangent> shapes::rectangleVPNTT(DirectX::SimpleMath::Vector2 size)
{
	vector<VertexPositionNormalTextureTangent> ret_vertex;
	ret_vertex.push_back(VertexPositionNormalTextureTangent(Vector3(0, 0, 0), Vector3(0, 0, 1), Vector2(0, 1), Vector3(0, 1, 0)));
	ret_vertex.push_back(VertexPositionNormalTextureTangent(Vector3(0, size.y, 0), Vector3(0, 0, 1), Vector2(0, 0), Vector3(0, 1, 0)));
	ret_vertex.push_back(VertexPositionNormalTextureTangent(Vector3(size.x, size.y, 0), Vector3(0, 0, 1), Vector2(1, 0), Vector3(0, 1, 0)));
	ret_vertex.push_back(VertexPositionNormalTextureTangent(Vector3(0, 0, 0), Vector3(0, 0, 1), Vector2(0, 1), Vector3(0, 1, 0)));
	ret_vertex.push_back(VertexPositionNormalTextureTangent(Vector3(size.x, size.y, 0), Vector3(0, 0, 1), Vector2(1, 0), Vector3(0, 1, 0)));
	ret_vertex.push_back(VertexPositionNormalTextureTangent(Vector3(size.x, 0, 0), Vector3(0, 0, 1), Vector2(1, 1), Vector3(0, 1, 0)));


	return ret_vertex;
}

std::vector<DirectX::VertexPositionNormalTexture> shapes::plainVPNT(plain pl)
{
	//tworzenie plaszczyzny z listy punktow z liczeniem normalnych i wartosci UV
	vector<VertexPositionNormalTexture> ret_vertex;
	vector<triangle> triangles = plainTriangles(pl);

	ret_vertex.reserve(3 * triangles.size());
	for (int i = 0; i < triangles.size(); i++)
	{
		
		ret_vertex.push_back(VertexPositionNormalTexture(triangles[i].vertex[0], triangles[i].normal[0], triangles[i].tex[0]));
		ret_vertex.push_back(VertexPositionNormalTexture(triangles[i].vertex[1], triangles[i].normal[1], triangles[i].tex[1]));
		ret_vertex.push_back(VertexPositionNormalTexture(triangles[i].vertex[2], triangles[i].normal[2], triangles[i].tex[2]));
	}
	
	return ret_vertex;
	

}

std::vector<VertexPositionNormalTextureTangent> shapes::plainVPNTT(plain pl)
{
	//tworzenie plaszczyzny z listy punktow z liczeniem normalnych i wartosci UV
	vector<VertexPositionNormalTextureTangent> ret_vertex;
	vector<triangle> triangles = plainTriangles(pl);

	ret_vertex.reserve(3 * triangles.size());
	for (int i = 0; i < triangles.size(); i++)
	{
		ret_vertex.push_back(VertexPositionNormalTextureTangent(triangles[i].vertex[0], triangles[i].normal[0], triangles[i].tex[0], triangles[i].tangent[0], triangles[i].foam[0]));
		ret_vertex.push_back(VertexPositionNormalTextureTangent(triangles[i].vertex[1], triangles[i].normal[1], triangles[i].tex[1], triangles[i].tangent[1], triangles[i].foam[1]));
		ret_vertex.push_back(VertexPositionNormalTextureTangent(triangles[i].vertex[2], triangles[i].normal[2], triangles[i].tex[2], triangles[i].tangent[2], triangles[i].foam[2]));
		
	}

	return ret_vertex;
}



std::vector<shapes::triangle> shapes::basicSphere()
{
	float t = (1.0 + sqrt(5.0)) / 2.0;
	vector<triangle> triangles;
	triangles.reserve(20);

	//podstawowe 20 trojkatow
	triangles.push_back(triangle(Vector3(-1, t, 0), Vector3(1, t, 0), Vector3(0, 1, t)));
	triangles.push_back(triangle(Vector3(-1, t, 0), Vector3(0, 1, -t), Vector3(1, t, 0)));
	triangles.push_back(triangle(Vector3(-1, t, 0), Vector3(0, 1, t), Vector3(-t, 0, 1)));
	triangles.push_back(triangle(Vector3(-1, t, 0), Vector3(-t, 0, 1), Vector3(-t, 0, -1)));
	triangles.push_back(triangle(Vector3(-1, t, 0), Vector3(-t, 0, -1), Vector3(0, 1, -t)));

	triangles.push_back(triangle(Vector3(0, -1, t), Vector3(-t, 0, 1), Vector3(0, 1, t)));
	triangles.push_back(triangle(Vector3(0, -1, t), Vector3(0, 1, t), Vector3(t, 0, 1)));
	triangles.push_back(triangle(Vector3(0, -1, t), Vector3(t, 0, 1), Vector3(1, -t, 0)));
	triangles.push_back(triangle(Vector3(0, -1, t), Vector3(1, -t, 0), Vector3(-1, -t, 0)));
	triangles.push_back(triangle(Vector3(0, -1, t), Vector3(-1, -t, 0), Vector3(-t, 0, 1)));

	triangles.push_back(triangle(Vector3(1, t, 0), Vector3(0, 1, -t), Vector3(t, 0, -1)));
	triangles.push_back(triangle(Vector3(0, 1, -t), Vector3(0, -1, -t), Vector3(t, 0, -1)));
	triangles.push_back(triangle(Vector3(0, 1, -t), Vector3(-t, 0, -1), Vector3(0, -1, -t)));
	triangles.push_back(triangle(Vector3(-t, 0, -1), Vector3(-t, 0, 1), Vector3(-1, -t, 0)));
	triangles.push_back(triangle(Vector3(-t, 0, -1), Vector3(-1, -t, 0), Vector3(0, -1, -t)));

	triangles.push_back(triangle(Vector3(0, -1, -t), Vector3(-1, -t, 0), Vector3(1, -t, 0)));
	triangles.push_back(triangle(Vector3(0, -1, -t), Vector3(1, -t, 0), Vector3(t, 0, -1)));
	triangles.push_back(triangle(Vector3(1, -t, 0), Vector3(t, 0, 1), Vector3(t, 0, -1)));
	triangles.push_back(triangle(Vector3(t, 0, -1), Vector3(t, 0, 1), Vector3(1, t, 0)));
	triangles.push_back(triangle(Vector3(1, t, 0), Vector3(t, 0, 1), Vector3(0, 1, t)));

	for (int i = 0; i < triangles.size(); i++)
	{
		triangles[i].vertex[0] = triangles[i].vertex[0] * 0.526f;
		triangles[i].vertex[1] = triangles[i].vertex[1] * 0.526f;
		triangles[i].vertex[2] = triangles[i].vertex[2] * 0.526f;		
	}

	return triangles;
}

std::vector<shapes::triangle> shapes::refine(std::vector<triangle> triangles)
{
	std::vector<triangle> temp;
	Vector3 vertex01, vertex12, vertex20;
	for (int i = 0; i < triangles.size(); i++)
	{
		vertex01 = getMiddle(triangles[i].vertex[0], triangles[i].vertex[1]);
		vertex12 = getMiddle(triangles[i].vertex[1], triangles[i].vertex[2]);
		vertex20 = getMiddle(triangles[i].vertex[2], triangles[i].vertex[0]);

		temp.push_back(triangle(triangles[i].vertex[0], vertex01, vertex20));
		temp.push_back(triangle(vertex20, vertex01, vertex12));
		temp.push_back(triangle(vertex01, triangles[i].vertex[1], vertex12));
		temp.push_back(triangle(vertex20, vertex12, triangles[i].vertex[2]));



	}
	return temp;
}

Vector3 shapes::getMiddle(Vector3 a, Vector3 b)
{
	Vector3 res = (a + b) / 2;
	res.Normalize();
	return res;
}

std::vector<shapes::triangle> shapes::plainTriangles(plain pl)
{
	vector<triangle> triangles;
	triangle temp;
	Vector3 temp1, temp2; //do obliczania wektora normalnego
	float skala_tekstury = 0.5f;
	for (int i = 0; i < pl.m_stepsZ -1 ; i++)
	{
		
		for (int j = 0; j < pl.m_stepsX - 1; j++)
		{

			
			//tworzenie trojkatow z siatki punktow
			/*temp.vertex[0] = Vector3(pl.points[pl.m_stepsZ*i + j].x, pl.points[pl.m_stepsZ*i + j].y, pl.points[pl.m_stepsZ*i + j].z);
			temp.vertex[1] = Vector3(pl.points[pl.m_stepsZ*(i + 1) + j].x, pl.points[pl.m_stepsZ*(i + 1) + j].y, pl.points[pl.m_stepsZ*(i + 1) + j].z);
			temp.vertex[2] = Vector3(pl.points[pl.m_stepsZ*i + j + 1].x, pl.points[pl.m_stepsZ*i + j + 1].y, pl.points[pl.m_stepsZ*i + j + 1].z);*/
			temp.vertex[0] = pl.points[pl.m_stepsX*i + j];
			temp.vertex[1] = pl.points[pl.m_stepsX*i + j + 1];
			temp.vertex[2] = pl.points[pl.m_stepsX*(i + 1) + j];
			temp.foam[0] = pl.foam[pl.m_stepsX*i + j];
			temp.foam[1] = pl.foam[pl.m_stepsX*i + j + 1];
			temp.foam[2] = pl.foam[pl.m_stepsX*(i + 1) + j];

			temp.tex[0] = pl.texCoord[pl.m_stepsX*i + j] *skala_tekstury;
			temp.tex[1] = pl.texCoord[pl.m_stepsX*i + j + 1] * skala_tekstury;
			temp.tex[2] = pl.texCoord[pl.m_stepsX*(i + 1) + j] * skala_tekstury;

			//wyliczanie wektora normalnego i stycznego dla trojkata
			temp.setNormal();
			temp.setTangent();
			triangles.push_back(temp);

			temp.vertex[0] = pl.points[pl.m_stepsX*i + j + 1];				
			temp.vertex[1] = pl.points[pl.m_stepsX*(i + 1) + j + 1];
			temp.vertex[2] = pl.points[pl.m_stepsX*(i + 1) + j];

			temp.foam[0] = pl.foam[pl.m_stepsX*i + j + 1];
			temp.foam[1] = pl.foam[pl.m_stepsX*(i + 1) + j + 1];
			temp.foam[2] = pl.foam[pl.m_stepsX*(i + 1) + j]; 

			temp.tex[0] = pl.texCoord[pl.m_stepsX*i + j + 1] * skala_tekstury;
			temp.tex[1] = pl.texCoord[pl.m_stepsX*(i + 1) + j + 1] * skala_tekstury;
			temp.tex[2] = pl.texCoord[pl.m_stepsX*(i + 1) + j] * skala_tekstury;

			temp.setNormal();
			temp.setTangent();
			triangles.push_back(temp);
				
		}
	}
	//wygladzanie normalnych przez wyliczanie srednich
	int it = 0;
	int divider;
	for (int i = 0; i < pl.m_stepsZ -1 ; i++)
	{
		for (int j = 0; j < pl.m_stepsX -1 ; j++)
		{
			//punkt 1
			
			divider = 1;			
			temp1 = triangles[it].triNormal;
			temp2 = triangles[it].triTangent;
			if (j > 0)
			{
				temp1 = temp1 + triangles[it - 2].triNormal;
				temp1 = temp1 + triangles[it - 1].triNormal;
				temp2 = temp2 + triangles[it - 2].triTangent;
				temp2 = temp2 + triangles[it - 1].triTangent;
				divider = divider + 2;
			}
			if (i > 0)
			{
				temp1 = temp1 + triangles[it - (2 * (pl.m_stepsX - 1))].triNormal;
				temp1 = temp1 + triangles[it - (2 * (pl.m_stepsX - 1)) + 1].triNormal;
				temp2 = temp2 + triangles[it - (2 * (pl.m_stepsX - 1))].triTangent;
				temp2 = temp2 + triangles[it - (2 * (pl.m_stepsX - 1)) + 1].triTangent;
				divider = divider + 2;
			}
			if ((j > 0) && (i > 0))
			{
				temp1 = temp1 + triangles[it - (2 * (pl.m_stepsX - 1)) - 1].triNormal;
				temp2 = temp2 + triangles[it - (2 * (pl.m_stepsX - 1)) - 1].triTangent;
				divider = divider + 1;
			}
			triangles[it].normal[0] = temp1 / divider;
			triangles[it].tangent[0] = temp2 / divider;
			
			//punkt 2
			divider = 2;
			temp1 = triangles[it].triNormal;
			temp1 = temp1 + triangles[it + 1].triNormal;
			temp2 = triangles[it].triTangent;
			temp2 = temp2 + triangles[it + 1].triTangent;
			if (j > 0)
			{
				temp1 = temp1 + triangles[it - 1].triNormal;
				temp2 = temp2 + triangles[it - 1].triTangent;
				divider = divider + 1;
			}
			if (i < pl.m_stepsZ - 2)
			{
				temp1 = temp1 + triangles[it + (2 * (pl.m_stepsX - 1))].triNormal;
				temp2 = temp2 + triangles[it + (2 * (pl.m_stepsX - 1))].triTangent;
				divider = divider + 1;
			}
			if ((j > 0) && (i < pl.m_stepsZ - 2))
			{
				temp1 = temp1 + triangles[it + (2 * (pl.m_stepsX - 1)) - 1].triNormal;
				temp1 = temp1 + triangles[it + (2 * (pl.m_stepsX - 1)) - 2].triNormal;
				temp2 = temp2 + triangles[it + (2 * (pl.m_stepsX - 1)) - 1].triTangent;
				temp2 = temp2 + triangles[it + (2 * (pl.m_stepsX - 1)) - 2].triTangent;
				divider = divider + 2;
			}
			temp1 = temp1 / divider;
			temp2 = temp2 / divider;
			triangles[it].normal[2] = temp1;
			triangles[it + 1].normal[2] = temp1;
			triangles[it].tangent[2] = temp2;
			triangles[it + 1].tangent[2] = temp2;

			//punkt 3
			divider = 2;
			temp1 = triangles[it].triNormal;
			temp1 = temp1 + triangles[it + 1].triNormal;
			temp2 = triangles[it].triTangent;
			temp2 = temp2 + triangles[it + 1].triTangent;
			if (j < pl.m_stepsX - 2)
			{
				temp1 = temp1 + triangles[it + 2].triNormal;
				temp2 = temp2 + triangles[it + 2].triTangent;
				divider = divider + 1;
			}
			if (i > 0)
			{
				temp1 = temp1 + triangles[it - (2 * (pl.m_stepsX - 1)) + 1].triNormal;
				temp2 = temp2 + triangles[it - (2 * (pl.m_stepsX - 1)) + 1].triTangent;
				divider = divider + 1;
			}
			if ((i > 0) && (j < pl.m_stepsX - 2))
			{
				temp1 = temp1 + triangles[it - (2 * (pl.m_stepsX - 1)) + 2].triNormal;
				temp1 = temp1 + triangles[it - (2 * (pl.m_stepsX - 1)) + 3].triNormal;
				temp2 = temp2 + triangles[it - (2 * (pl.m_stepsX - 1)) + 2].triTangent;
				temp2 = temp2 + triangles[it - (2 * (pl.m_stepsX - 1)) + 3].triTangent;
				divider = divider + 2;
			}
			temp1 = temp1 / divider;
			temp2 = temp2 / divider;
			triangles[it].normal[1] = temp1;
			triangles[it + 1].normal[0] = temp1;
			triangles[it].tangent[1] = temp2;
			triangles[it + 1].tangent[0] = temp2;

			//punkt 4
			divider = 1;
			temp1 = triangles[it + 1].triNormal;
			temp2 = triangles[it + 1].triTangent;
			if (j < pl.m_stepsX - 2)
			{
				temp1 = temp1 + triangles[it + 2].triNormal;
				temp1 = temp1 + triangles[it + 3].triNormal;
				temp2 = temp2 + triangles[it + 2].triTangent;
				temp2 = temp2 + triangles[it + 3].triTangent;
				divider = divider + 2;
			}
			if (i < pl.m_stepsZ - 2)
			{
				temp1 = temp1 + triangles[it + (2 * (pl.m_stepsX - 1))].triNormal;
				temp1 = temp1 + triangles[it + (2 * (pl.m_stepsX - 1)) + 1].triNormal;
				temp2 = temp2 + triangles[it + (2 * (pl.m_stepsX - 1))].triTangent;
				temp2 = temp2 + triangles[it + (2 * (pl.m_stepsX - 1)) + 1].triTangent;
				divider = divider + 2;
			}
			if ((j < pl.m_stepsX - 2) && (i < pl.m_stepsZ - 2))
			{
				temp1 = temp1 + triangles[it + (2 * (pl.m_stepsX - 1)) +2].triNormal;
				temp2 = temp2 + triangles[it + (2 * (pl.m_stepsX - 1)) + 2].triTangent;
				divider = divider + 1;
			}
			triangles[it + 1].normal[1] = temp1 / divider;
			triangles[it + 1].tangent[1] = temp2 / divider;

			it = it + 2;
		}
	}

	return triangles;
}



plain::plain(DirectX::SimpleMath::Vector2 size, int steps_x, int steps_z)
{
	m_stepsX = steps_x +1;
	m_stepsZ = steps_z +1;
	
	float start_x = 0;
	float start_z = 0;
	float step_x = size.x;
	float step_z = size.y;
	for (int j = 0; j <= steps_z; j++)
	{
		for (int i = 0; i <= steps_x; i++)
		{
		
			points.push_back(Vector3(start_x + (i*step_x), 0, start_z + (j*step_z)));
			texCoord.push_back(Vector2(i*step_x, j*step_z));
			if (j < 4)
			{
				foam.push_back(Vector3(0,0,0));   //temp
			}
			else
			{
				foam.push_back(Vector3(0, 0, 0));
			}
		}
	}

}

plain::plain(DirectX::SimpleMath::Vector2 size, int steps_x, int steps_z, int steps_y)
{
	m_stepsX = steps_x + 1;
	m_stepsZ = steps_z + steps_y + 1;

	float start_x = 0;
	float start_z = 0;
	float step_x = size.x;
	float step_z = size.y;
	for (int j = 0; j <= steps_z; j++)
	{
		for (int i = 0; i <= steps_x; i++)
		{
			points.push_back(Vector3(start_x + (i*step_x), 0, start_z + (j*step_z)));
			texCoord.push_back(Vector2(i*step_x, j*step_z));
			
			if (j > steps_z - 1)
			{
				foam.push_back(Vector3(0.5f, 0, 0));   //temp
			}
			else
			{
				foam.push_back(Vector3(0, 0, 0));
			}
			
		}
	}
	for (int j = 1; j <= steps_y-1; j++)
	{
		for (int i = 0; i <= steps_x; i++)
		{
			points.push_back(Vector3(start_x + (i*step_x), -(j*step_z), j*0.03f + step_z + (steps_z*step_z)));
			texCoord.push_back(Vector2(i*step_x, (steps_z+j)*step_z));
			foam.push_back(Vector3(1, 0, 0));

		}
	}
	for (int i = 0; i <= steps_x; i++)
	{
		points.push_back(Vector3(start_x + (i*step_x), -(steps_y*step_z), steps_y*0.03f + step_z + (steps_z*step_z)));
		texCoord.push_back(Vector2(i*step_x, (steps_z + steps_y)*step_z));
		foam.push_back(Vector3(1, 1, 0));

	}
}

void plain::wave()
{
	for (int i = 0; i < m_stepsX; i++)
	{
		for (int j = 0; j < m_stepsZ; j++)
		{
			points[m_stepsZ*i + j].y = 0.05f * (sinf(10.0f*(points[m_stepsZ*i + j].x)) + 0.5f* (sinf(5.0f*(points[m_stepsZ*i + j].z))));
		}
	}
}

std::vector<VertexPositionNormalTextureTangent> shapes::upperWaterModel(float height, float length, float width, float depth, float speed, DirectX::SimpleMath::Plane collPlane)
{
	vector<vector<point>> plPoints = waterPlainPoints(length, width);
	vector<vector<point>> dropPoints = waterDropPoints( height, width, speed, collPlane); //opadajaca czesc
	vector<vector<point>> sidesPoints = waterSidesPoints(height, depth, speed, collPlane, &dropPoints); //boki
	vector<vector<quad>> plQuads = waterPlainTriangles(plPoints);
	vector<vector<quad>> dropQuads = waterPlainTriangles(dropPoints);
	vector<vector<triangle>> sideTriangles = waterSidesTriangles(dropPoints, sidesPoints);
	smoothWaterPlain(plQuads,dropQuads);
	smoothWaterDropSides(dropQuads, sideTriangles, plQuads);

	vector<VertexPositionNormalTextureTangent> ret_vertex;

	quadsToVertex(plQuads, ret_vertex);
	quadsToVertex(dropQuads, ret_vertex);
	trianglesToVertex(sideTriangles, ret_vertex);

	return ret_vertex;
}

std::vector<std::vector<shapes::point>> shapes::waterPlainPoints(float length, float width)
{
	vector<vector<point>> allRows;
	vector<point> row;
	point temp_point;

	temp_point.pos = Vector3(-width / 2, 0, -length);
	temp_point.foam = Vector3(0, 0, 0);
	temp_point.texCoord = TEX_SCALE * Vector2(temp_point.pos.x, temp_point.pos.z);
	int parts_x = 4;
	for (int i = 0; i < parts_x +1; i++)
	{
		row.push_back(temp_point);
		temp_point.pos.x = temp_point.pos.x + width / parts_x;
		temp_point.texCoord = TEX_SCALE * Vector2(temp_point.pos.x, temp_point.pos.z);
	}
	allRows.push_back(row);

	int parts_z = 6;
	for (int i = 1; i < parts_z+1 ; i++) 
	{
		for (int j = 0; j < parts_x + 1; j++)
		{
			row[j].pos.z = row[j].pos.z + length / parts_z;
			row[j].texCoord = TEX_SCALE * Vector2(row[j].pos.x, row[j].pos.z);
		}
		allRows.push_back(row);
	}


	return allRows;
}

std::vector<std::vector<shapes::point>> shapes::waterDropPoints(float height, float width, float speed, DirectX::SimpleMath::Plane collPlane)
{
	vector<vector<point>> allRows;
	vector<point> row,row2;
	point temp_point;

	temp_point.pos = Vector3(-width / 2, 0, 0);
	temp_point.foam = Vector3(0, 0, 0);
	temp_point.texCoord = TEX_SCALE * Vector2(temp_point.pos.x, temp_point.pos.z);
	int parts_x = 4;
	for (int i = 0; i < parts_x + 1; i++)
	{
		row.push_back(temp_point);
		temp_point.pos.x = temp_point.pos.x + width / parts_x;
		temp_point.texCoord = TEX_SCALE * Vector2(temp_point.pos.x, temp_point.pos.z);
	}
	allRows.push_back(row);
	
	row2.resize(parts_x + 1);

	float t = 0;	
	Vector3 fallingPos;
	float dist;
	
	int i = 1;
	while(fallingPos.y > -(height+0.1f))
	{
		if (i <= 10)
			t = t + 0.05f;
		else
			t = t + 0.1f;

		fallingPos = 0.5f*t*t*Vector3(0, -10.0f, 0) + t*Vector3(0, 0, speed);
		
		dist = collPlane.DotCoordinate(fallingPos);
		
		if (dist < 0) //moze dozmiany
		{
			fallingPos -= dist  * collPlane.Normal();
		}



		for (int j = 0; j < parts_x + 1; j++)
		{
			row2[j].pos = row[j].pos + fallingPos;
			row2[j].foam = Vector3(1, 0, 0);
			row2[j].texCoord = row[j].texCoord + FALL_TEX_SCALE * Vector2(0, t);
		}
		allRows.push_back(row2);
		i++;
	}
	

	return allRows;
}

std::vector<std::vector<shapes::point>> shapes::waterSidesPoints(float height, float depth, float speed, DirectX::SimpleMath::Plane collPlane, std::vector<std::vector<point>>* dropPoints)
{
	vector<vector<point>> sides;
	vector<point> side;
	
	point temp_point;
	Vector3 fallingPos;
	float dist;
	
	float t = 0;
	float t_f = sqrt(2 * height - depth / 10.0f) + 0.1f;

	int size_y = (*dropPoints).size();
	float x_poczatkowy = ((*dropPoints)[0]).front().pos.x;
	
	temp_point.pos = Vector3(x_poczatkowy, -depth, 0);
	temp_point.foam = Vector3(1, 0, 0);
	Vector2 texCoord0 = Vector2((x_poczatkowy - depth)* FALL_TEX_SCALE, 0);
	temp_point.texCoord = texCoord0;
	side.push_back(temp_point);
	for (int i = 1; i < size_y ; i++)
	{
		if (i <= 10)
			t = t + 0.05f;
		else
			t = t + 0.1f;

		fallingPos = 0.5f*t*t*Vector3(0, -10.0f, 0) + t*Vector3(0, 0, speed) + Vector3(x_poczatkowy, -depth, 0);

		dist = collPlane.DotCoordinate(fallingPos);

		if (dist < -0.1) //moze dozmiany
		{
			fallingPos -= (dist+0.1)  * collPlane.Normal();
						
		}


		temp_point.pos = fallingPos;
		temp_point.texCoord = texCoord0 + FALL_TEX_SCALE * Vector2(0, t);
		side.push_back(temp_point);			
			
		
	}
	sides.push_back(side);
	float x_koncowy = ((*dropPoints)[0]).back().pos.x;
	for (int i = 0; i < side.size(); i++)
	{
		side[i].pos.x = x_koncowy;
		side[i].texCoord.x = (x_koncowy - depth)* FALL_TEX_SCALE;
	}
	sides.push_back(side);
	return sides;
}

std::vector<std::vector<shapes::quad>> shapes::waterPlainTriangles(std::vector<std::vector<point>> points)
{
	vector<vector<quad>> quads;
	vector<quad> row;
	int y = points.size() - 1;
	int x = points[0].size() - 1;
	
	row.resize(x);
	for (int i = 0; i < y; i++)
	{
		for (int j = 0; j < x; j++)
		{
			row[j].upperLeft.vertex[0] = points[i][j].pos;
			row[j].upperLeft.vertex[1] = points[i][j + 1].pos;
			row[j].upperLeft.vertex[2] = points[i + 1][j].pos;
			row[j].upperLeft.tex[0] = points[i][j].texCoord;
			row[j].upperLeft.tex[1] = points[i][j + 1].texCoord;
			row[j].upperLeft.tex[2] = points[i + 1][j].texCoord;
			row[j].upperLeft.foam[0] = points[i][j].foam;
			row[j].upperLeft.foam[1] = points[i][j + 1].foam;
			row[j].upperLeft.foam[2] = points[i + 1][j].foam;
			row[j].upperLeft.setNormal();
			row[j].upperLeft.setTangent();

			//temp
			row[j].upperLeft.normal[0] = row[j].upperLeft.triNormal;
			row[j].upperLeft.normal[1] = row[j].upperLeft.triNormal;
			row[j].upperLeft.normal[2] = row[j].upperLeft.triNormal;
			row[j].upperLeft.tangent[0] = row[j].upperLeft.triTangent;
			row[j].upperLeft.tangent[1] = row[j].upperLeft.triTangent;
			row[j].upperLeft.tangent[2] = row[j].upperLeft.triTangent;
			



			row[j].lowerRight.vertex[0] = points[i][j + 1].pos;
			row[j].lowerRight.vertex[1] = points[i + 1][j + 1].pos;
			row[j].lowerRight.vertex[2] = points[i + 1][j].pos;
			row[j].lowerRight.tex[0] = points[i][j + 1].texCoord;
			row[j].lowerRight.tex[1] = points[i + 1][j + 1].texCoord;
			row[j].lowerRight.tex[2] = points[i + 1][j].texCoord;
			row[j].lowerRight.foam[0] = points[i][j + 1].foam;
			row[j].lowerRight.foam[1] = points[i + 1][j + 1].foam;
			row[j].lowerRight.foam[2] = points[i + 1][j].foam;
			row[j].lowerRight.setNormal();
			row[j].lowerRight.setTangent();


			//temp
			row[j].lowerRight.normal[0] = row[j].lowerRight.triNormal;
			row[j].lowerRight.normal[1] = row[j].lowerRight.triNormal;
			row[j].lowerRight.normal[2] = row[j].lowerRight.triNormal;
			row[j].lowerRight.tangent[0] = row[j].lowerRight.triTangent;
			row[j].lowerRight.tangent[1] = row[j].lowerRight.triTangent;
			row[j].lowerRight.tangent[2] = row[j].lowerRight.triTangent;

		}
		quads.push_back(row);
	}



	return quads;
}

std::vector<std::vector<shapes::triangle>> shapes::waterSidesTriangles(std::vector<std::vector<point>> dropPoints, std::vector<std::vector<point>> sidePoints)
{
	vector<vector<triangle>> triangles;
	vector<triangle> row;
	triangle temp;
	
	int x = dropPoints.size() - 1;
	
	

	for (int i = 0; i < x; i++)
	{
		temp.vertex[0] = dropPoints[i][0].pos;
		temp.vertex[1] = dropPoints[i+1][0].pos;
		temp.vertex[2] = sidePoints[0][i].pos;
		temp.tex[0] = dropPoints[i][0].texCoord;
		temp.tex[1] = dropPoints[i + 1][0].texCoord;
		temp.tex[2] = sidePoints[0][i].texCoord;
		temp.foam[0] = dropPoints[i][0].foam;
		temp.foam[1] = dropPoints[i + 1][0].foam;
		temp.foam[2] = sidePoints[0][i].foam;
		temp.setNormal();
		temp.setTangent();
		row.push_back(temp);

		temp.vertex[0] = sidePoints[0][i].pos;		
		temp.vertex[1] = dropPoints[i + 1][0].pos;
		temp.vertex[2] = sidePoints[0][i + 1].pos;
		temp.tex[0] = sidePoints[0][i].texCoord;
		temp.tex[1] = dropPoints[i + 1][0].texCoord;
		temp.tex[2] = sidePoints[0][i + 1].texCoord;
		temp.foam[0] = sidePoints[0][i].foam;
		temp.foam[1] = dropPoints[i + 1][0].foam;
		temp.foam[2] = sidePoints[0][i + 1].foam;
		temp.setNormal();
		temp.setTangent();
		row.push_back(temp);
	}
	
	triangles.push_back(row);
	
	row.clear();
	for (int i = 0; i < x; i++)
	{
		temp.vertex[0] = dropPoints[i+1].back().pos;
		temp.vertex[1] = dropPoints[i].back().pos;
		temp.vertex[2] = sidePoints[1][i].pos;
		temp.tex[0] = dropPoints[i + 1].back().texCoord;
		temp.tex[1] = dropPoints[i].back().texCoord;
		temp.tex[2] = sidePoints[1][i].texCoord;
		temp.foam[0] = dropPoints[i + 1].back().foam;
		temp.foam[1] = dropPoints[i].back().foam;
		temp.foam[2] = sidePoints[1][i].foam;
		temp.setNormal();
		temp.setTangent();
		row.push_back(temp);

		temp.vertex[0] = sidePoints[1][i+1].pos;
		temp.vertex[1] = dropPoints[i + 1].back().pos;
		temp.vertex[2] = sidePoints[1][i].pos;
		temp.tex[0] = sidePoints[1][i + 1].texCoord;
		temp.tex[1] = dropPoints[i + 1].back().texCoord;
		temp.tex[2] = sidePoints[1][i].texCoord;
		temp.foam[0] = sidePoints[1][i + 1].foam;
		temp.foam[1] = dropPoints[i + 1].back().foam;
		temp.foam[2] = sidePoints[1][i].foam;
		temp.setNormal();
		temp.setTangent();
		row.push_back(temp);
	}
	
	triangles.push_back(row);

	return triangles;
}

void shapes::smoothWaterPlain(std::vector<std::vector<quad>>& plain, std::vector<std::vector<quad>>& drop)
{
	Vector3 temp_normal;
	Vector3 temp_tangent;
	int tri_num;
	for (int i = 0; i < plain.size(); i++)
	{
		for (int j = 0; j < plain[0].size(); j++)
		{

			temp_normal = Vector3(0.0f);
			temp_tangent = Vector3(0.0f);
			tri_num = 0;
			//gorny lewy
			if (i != 0 && j != 0)
			{
				temp_normal = temp_normal + plain[i - 1][j - 1].lowerRight.triNormal;
				temp_tangent = temp_tangent + plain[i - 1][j - 1].lowerRight.triTangent;
				tri_num++;
			}
			if (i != 0 )
			{
				temp_normal = temp_normal + plain[i - 1][j].lowerRight.triNormal;
				temp_tangent = temp_tangent + plain[i - 1][j ].lowerRight.triTangent;
				temp_normal = temp_normal + plain[i - 1][j].upperLeft.triNormal;
				temp_tangent = temp_tangent + plain[i - 1][j].upperLeft.triTangent;
				tri_num=tri_num+2;
			}
			if (j != 0)
			{
				temp_normal = temp_normal + plain[i][j-1].lowerRight.triNormal;
				temp_tangent = temp_tangent + plain[i][j-1].lowerRight.triTangent;
				temp_normal = temp_normal + plain[i][j-1].upperLeft.triNormal;
				temp_tangent = temp_tangent + plain[i][j-1].upperLeft.triTangent;
				tri_num = tri_num + 2;
			}
			temp_normal = temp_normal + plain[i][j].upperLeft.triNormal;
			temp_tangent = temp_tangent + plain[i][j].upperLeft.triTangent;
			tri_num = tri_num + 1;
			temp_normal = temp_normal / tri_num;
			temp_tangent = temp_tangent / tri_num;

			plain[i][j].upperLeft.normal[0] = temp_normal;
			plain[i][j].upperLeft.tangent[0] = temp_tangent;
			//------------------------------------------




			temp_normal = Vector3(0.0f);
			temp_tangent = Vector3(0.0f);
			tri_num = 0;
			//gorny prawy

			if (i != 0 && j != plain[0].size()-1)
			{
				temp_normal = temp_normal + plain[i - 1][j + 1].lowerRight.triNormal;
				temp_tangent = temp_tangent + plain[i - 1][j + 1].lowerRight.triTangent;
				temp_normal = temp_normal + plain[i - 1][j + 1].upperLeft.triNormal;
				temp_tangent = temp_tangent + plain[i - 1][j + 1].upperLeft.triTangent;
				tri_num = tri_num + 2;
			}
			if (i != 0)
			{
				temp_normal = temp_normal + plain[i - 1][j].lowerRight.triNormal;
				temp_tangent = temp_tangent + plain[i - 1][j].lowerRight.triTangent;
				tri_num = tri_num + 1;
			}
			if (j != plain[0].size()-1)
			{
				
				temp_normal = temp_normal + plain[i][j + 1].upperLeft.triNormal;
				temp_tangent = temp_tangent + plain[i][j + 1].upperLeft.triTangent;
				tri_num = tri_num + 1;
			}

			temp_normal = temp_normal + plain[i][j].upperLeft.triNormal;
			temp_tangent = temp_tangent + plain[i][j].upperLeft.triTangent;
			temp_normal = temp_normal + plain[i][j].lowerRight.triNormal;
			temp_tangent = temp_tangent + plain[i][j].lowerRight.triTangent;
			tri_num = tri_num + 2;
			temp_normal = temp_normal / tri_num;
			temp_tangent = temp_tangent / tri_num;

			plain[i][j].upperLeft.normal[1] = temp_normal;
			plain[i][j].upperLeft.tangent[1] = temp_tangent;
			plain[i][j].lowerRight.normal[0] = temp_normal;
			plain[i][j].lowerRight.tangent[0] = temp_tangent;
			//------------------------------------------------------




			temp_normal = Vector3(0.0f);
			temp_tangent = Vector3(0.0f);
			tri_num = 0;
			//dolny lewy
			if (i != plain.size()-1 && j != 0)
			{
				temp_normal = temp_normal + plain[i + 1][j - 1].lowerRight.triNormal;
				temp_tangent = temp_tangent + plain[i + 1][j - 1].lowerRight.triTangent;
				temp_normal = temp_normal + plain[i + 1][j - 1].upperLeft.triNormal;
				temp_tangent = temp_tangent + plain[i + 1][j - 1].upperLeft.triTangent;
				tri_num = tri_num + 2;
			}
			if (i != plain.size()-1)
			{
				temp_normal = temp_normal + plain[i + 1][j].upperLeft.triNormal;
				temp_tangent = temp_tangent + plain[i + 1][j].upperLeft.triTangent;
				tri_num = tri_num + 1;
			}
			else
			{
				if (j != 0)
				{
					temp_normal = temp_normal + drop[0][j - 1].lowerRight.triNormal;
					temp_tangent = temp_tangent + drop[0][j - 1].lowerRight.triTangent;
					temp_normal = temp_normal + drop[0][j - 1].upperLeft.triNormal;
					temp_tangent = temp_tangent + drop[0][j - 1].upperLeft.triTangent;
					tri_num = tri_num + 2;

				}
				temp_normal = temp_normal + drop[0][j].upperLeft.triNormal;
				temp_tangent = temp_tangent + drop[0][j].upperLeft.triTangent;
				tri_num = tri_num + 1;

			}
			if (j != 0)
			{

				temp_normal = temp_normal + plain[i][j - 1].lowerRight.triNormal;
				temp_tangent = temp_tangent + plain[i][j - 1].lowerRight.triTangent;
				tri_num = tri_num + 1;
			}

			temp_normal = temp_normal + plain[i][j].upperLeft.triNormal;
			temp_tangent = temp_tangent + plain[i][j].upperLeft.triTangent;
			temp_normal = temp_normal + plain[i][j].lowerRight.triNormal;
			temp_tangent = temp_tangent + plain[i][j].lowerRight.triTangent;
			tri_num = tri_num + 2;
			temp_normal = temp_normal / tri_num;
			temp_tangent = temp_tangent / tri_num;

			plain[i][j].upperLeft.normal[2] = temp_normal;
			plain[i][j].upperLeft.tangent[2] = temp_tangent;
			plain[i][j].lowerRight.normal[2] = temp_normal;
			plain[i][j].lowerRight.tangent[2] = temp_tangent;
			//------------------------------------------------------






			temp_normal = Vector3(0.0f);
			temp_tangent = Vector3(0.0f);
			tri_num = 0;
			//dolny prawy
			if (i != plain.size()-1 && j != plain[0].size()-1)
			{
				
				temp_normal = temp_normal + plain[i + 1][j + 1].upperLeft.triNormal;
				temp_tangent = temp_tangent + plain[i + 1][j + 1].upperLeft.triTangent;
				tri_num = tri_num + 1;
			}
			if (i != plain.size()-1)
			{
				temp_normal = temp_normal + plain[i + 1][j].upperLeft.triNormal;
				temp_tangent = temp_tangent + plain[i + 1][j].upperLeft.triTangent;
				tri_num = tri_num + 1;
			}
			else
			{
				if (j != plain[0].size()-1)
				{
					
					temp_normal = temp_normal + drop[0][j + 1].upperLeft.triNormal;
					temp_tangent = temp_tangent + drop[0][j + 1].upperLeft.triTangent;
					tri_num = tri_num + 1;

				}
				temp_normal = temp_normal + drop[0][j].upperLeft.triNormal;
				temp_tangent = temp_tangent + drop[0][j].upperLeft.triTangent;
				temp_normal = temp_normal + drop[0][j].lowerRight.triNormal;
				temp_tangent = temp_tangent + drop[0][j].lowerRight.triTangent;
				tri_num = tri_num + 2;

			}
			if (j != plain[0].size()-1)
			{

				temp_normal = temp_normal + plain[i][j + 1].upperLeft.triNormal;
				temp_tangent = temp_tangent + plain[i][j + 1].upperLeft.triTangent;
				temp_normal = temp_normal + plain[i][j + 1].lowerRight.triNormal;
				temp_tangent = temp_tangent + plain[i][j + 1].lowerRight.triTangent;
				tri_num = tri_num + 2;
			}


			temp_normal = temp_normal + plain[i][j].upperLeft.triNormal;
			temp_tangent = temp_tangent + plain[i][j].upperLeft.triTangent;
			tri_num = tri_num + 1;
			temp_normal = temp_normal / tri_num;
			temp_tangent = temp_tangent / tri_num;

			plain[i][j].lowerRight.normal[1] = temp_normal;
			plain[i][j].lowerRight.tangent[1] = temp_tangent;

		}
	}
}

void shapes::smoothWaterDropSides(std::vector<std::vector<quad>> & drop, std::vector<std::vector <triangle>> & sides, std::vector<std::vector<quad>> & plain)
{
	Vector3 temp_normal;
	Vector3 temp_tangent;
	int tri_num;
	for (int i = 0; i < drop.size(); i++)
	{
		for (int j = 0; j < drop[0].size(); j++)
		{

			temp_normal = Vector3(0.0f);
			temp_tangent = Vector3(0.0f);
			tri_num = 0;
			//gorny lewy
			if (i != 0 && j != 0)
			{
				temp_normal = temp_normal + drop[i - 1][j - 1].lowerRight.triNormal;
				temp_tangent = temp_tangent + drop[i - 1][j - 1].lowerRight.triTangent;
				tri_num++;
			}
			if (i != 0)
			{
				temp_normal = temp_normal + drop[i - 1][j].lowerRight.triNormal;
				temp_tangent = temp_tangent + drop[i - 1][j].lowerRight.triTangent;
				temp_normal = temp_normal + drop[i - 1][j].upperLeft.triNormal;
				temp_tangent = temp_tangent + drop[i - 1][j].upperLeft.triTangent;
				tri_num = tri_num + 2;
			}
			else
			{
				if (j != 0)
				{
					temp_normal = temp_normal + plain.back()[j - 1].lowerRight.triNormal;
					temp_tangent = temp_tangent + plain.back()[j - 1].lowerRight.triTangent;
					tri_num++;
				}
				temp_normal = temp_normal + plain.back()[j].lowerRight.triNormal;
				temp_tangent = temp_tangent + plain.back()[j].lowerRight.triTangent;
				temp_normal = temp_normal + plain.back()[j].upperLeft.triNormal;
				temp_tangent = temp_tangent + plain.back()[j].upperLeft.triTangent;
				tri_num = tri_num + 2;
			
			}
			if (j != 0)
			{
				temp_normal = temp_normal + drop[i][j - 1].lowerRight.triNormal;
				temp_tangent = temp_tangent + drop[i][j - 1].lowerRight.triTangent;
				temp_normal = temp_normal + drop[i][j - 1].upperLeft.triNormal;
				temp_tangent = temp_tangent + drop[i][j - 1].upperLeft.triTangent;
				tri_num = tri_num + 2;
			}
			else
			{
				if (i != 0)
				{
					temp_normal = temp_normal + sides[0][2 * (i-1)].triNormal;
					temp_tangent = temp_tangent + sides[0][2 * (i - 1)].triTangent;
					temp_normal = temp_normal + sides[0][2 * (i - 1) +1].triNormal;
					temp_tangent = temp_tangent + sides[0][2 * (i - 1)+1].triTangent;
					tri_num = tri_num + 2;

				}
				temp_normal = temp_normal + sides[0][2*i].triNormal;
				temp_tangent = temp_tangent + sides[0][2*i].triTangent; 
				tri_num = tri_num + 1;
			}
			temp_normal = temp_normal + drop[i][j].upperLeft.triNormal;
			temp_tangent = temp_tangent + drop[i][j].upperLeft.triTangent;
			tri_num = tri_num + 1;
			temp_normal = temp_normal / tri_num;
			temp_tangent = temp_tangent / tri_num;

			drop[i][j].upperLeft.normal[0] = temp_normal;
			drop[i][j].upperLeft.tangent[0] = temp_tangent;
			//------------------------------------------




			temp_normal = Vector3(0.0f);
			temp_tangent = Vector3(0.0f);
			tri_num = 0;
			//gorny prawy

			if (i != 0 && j != drop[0].size() - 1)
			{
				temp_normal = temp_normal + drop[i - 1][j + 1].lowerRight.triNormal;
				temp_tangent = temp_tangent + drop[i - 1][j + 1].lowerRight.triTangent;
				temp_normal = temp_normal + drop[i - 1][j + 1].upperLeft.triNormal;
				temp_tangent = temp_tangent + drop[i - 1][j + 1].upperLeft.triTangent;
				tri_num = tri_num + 2;
			}
			if (i != 0)
			{
				temp_normal = temp_normal + drop[i - 1][j].lowerRight.triNormal;
				temp_tangent = temp_tangent + drop[i - 1][j].lowerRight.triTangent;
				tri_num = tri_num + 1;
			}
			else
			{
				if (j != drop[0].size() - 1)
				{
					temp_normal = temp_normal + plain.back()[j +1].lowerRight.triNormal;
					temp_tangent = temp_tangent + plain.back()[j + 1].lowerRight.triTangent;
					temp_normal = temp_normal + plain.back()[j + 1].upperLeft.triNormal;
					temp_tangent = temp_tangent + plain.back()[j + 1].upperLeft.triTangent;
					tri_num = tri_num + 2;
				}
				temp_normal = temp_normal + plain.back()[j].lowerRight.triNormal;
				temp_tangent = temp_tangent + plain.back()[j].lowerRight.triTangent;
				
				tri_num = tri_num + 1;

			}
			if (j != drop[0].size() - 1)
			{

				temp_normal = temp_normal + drop[i][j + 1].upperLeft.triNormal;
				temp_tangent = temp_tangent + drop[i][j + 1].upperLeft.triTangent;
				tri_num = tri_num + 1;
			}
			else
			{
				if (i != 0)
				{
					temp_normal = temp_normal + sides[1][2 * (i - 1)].triNormal;
					temp_tangent = temp_tangent + sides[1][2 * (i - 1)].triTangent;
					temp_normal = temp_normal + sides[1][(2 * (i - 1)) + 1].triNormal;
					temp_tangent = temp_tangent + sides[1][(2 * (i - 1)) + 1].triTangent;
					tri_num = tri_num + 2;

				}
				temp_normal = temp_normal + sides[1][2 * i].triNormal;
				temp_tangent = temp_tangent + sides[1][2 * i].triTangent;
				tri_num = tri_num + 1;
			}

			temp_normal = temp_normal + drop[i][j].upperLeft.triNormal;
			temp_tangent = temp_tangent + drop[i][j].upperLeft.triTangent;
			temp_normal = temp_normal + drop[i][j].lowerRight.triNormal;
			temp_tangent = temp_tangent + drop[i][j].lowerRight.triTangent;
			tri_num = tri_num + 2;
			temp_normal = temp_normal / tri_num;
			temp_tangent = temp_tangent / tri_num;

			drop[i][j].upperLeft.normal[1] = temp_normal;
			drop[i][j].upperLeft.tangent[1] = temp_tangent;
			drop[i][j].lowerRight.normal[0] = temp_normal;
			drop[i][j].lowerRight.tangent[0] = temp_tangent;
			//------------------------------------------------------




			temp_normal = Vector3(0.0f);
			temp_tangent = Vector3(0.0f);
			tri_num = 0;
			//dolny lewy
			if (i != drop.size() - 1 && j != 0)
			{
				temp_normal = temp_normal + drop[i + 1][j - 1].lowerRight.triNormal;
				temp_tangent = temp_tangent + drop[i + 1][j - 1].lowerRight.triTangent;
				temp_normal = temp_normal + drop[i + 1][j - 1].upperLeft.triNormal;
				temp_tangent = temp_tangent + drop[i + 1][j - 1].upperLeft.triTangent;
				tri_num = tri_num + 2;
			}
			if (i != drop.size() - 1)
			{
				temp_normal = temp_normal + drop[i + 1][j].upperLeft.triNormal;
				temp_tangent = temp_tangent + drop[i + 1][j].upperLeft.triTangent;
				tri_num = tri_num + 1;
			}
			
			if (j != 0)
			{

				temp_normal = temp_normal + drop[i][j - 1].lowerRight.triNormal;
				temp_tangent = temp_tangent + drop[i][j - 1].lowerRight.triTangent;
				tri_num = tri_num + 1;
			}
			else
			{
				if (i != drop.size() - 1)
				{
					temp_normal = temp_normal + sides[0][2 * (i + 1)].triNormal;
					temp_tangent = temp_tangent + sides[0][2 * (i + 1)].triTangent;
					
					tri_num = tri_num +1;

				}
				temp_normal = temp_normal + sides[0][2 * i].triNormal;
				temp_tangent = temp_tangent + sides[0][2 * i].triTangent;
				temp_normal = temp_normal + sides[0][2 * i + 1].triNormal;
				temp_tangent = temp_tangent + sides[0][2 * i + 1].triTangent;
				tri_num = tri_num + 2;
			}

			temp_normal = temp_normal + drop[i][j].upperLeft.triNormal;
			temp_tangent = temp_tangent + drop[i][j].upperLeft.triTangent;
			temp_normal = temp_normal + drop[i][j].lowerRight.triNormal;
			temp_tangent = temp_tangent + drop[i][j].lowerRight.triTangent;
			tri_num = tri_num + 2;
			temp_normal = temp_normal / tri_num;
			temp_tangent = temp_tangent / tri_num;

			drop[i][j].upperLeft.normal[2] = temp_normal;
			drop[i][j].upperLeft.tangent[2] = temp_tangent;
			drop[i][j].lowerRight.normal[2] = temp_normal;
			drop[i][j].lowerRight.tangent[2] = temp_tangent;
			//------------------------------------------------------






			temp_normal = Vector3(0.0f);
			temp_tangent = Vector3(0.0f);
			tri_num = 0;
			//dolny prawy
			if (i != drop.size() - 1 && j != drop[0].size() - 1)
			{

				temp_normal = temp_normal + drop[i + 1][j + 1].upperLeft.triNormal;
				temp_tangent = temp_tangent + drop[i + 1][j + 1].upperLeft.triTangent;
				tri_num = tri_num + 1;
			}
			if (i != drop.size() - 1)
			{
				temp_normal = temp_normal + drop[i + 1][j].upperLeft.triNormal;
				temp_tangent = temp_tangent + drop[i + 1][j].upperLeft.triTangent;
				temp_normal = temp_normal + drop[i + 1][j].lowerRight.triNormal;
				temp_tangent = temp_tangent + drop[i + 1][j].lowerRight.triTangent;
				tri_num = tri_num + 2;
			}
			if (j != drop[0].size() - 1)
			{

				temp_normal = temp_normal + drop[i][j + 1].upperLeft.triNormal;
				temp_tangent = temp_tangent + drop[i][j + 1].upperLeft.triTangent;
				temp_normal = temp_normal + drop[i][j + 1].lowerRight.triNormal;
				temp_tangent = temp_tangent + drop[i][j + 1].lowerRight.triTangent;
				tri_num = tri_num + 2;
			}
			else 
			{
				if (i != drop.size() - 1)
				{
					temp_normal = temp_normal + sides[1][2 * (i + 1)].triNormal;
					temp_tangent = temp_tangent + sides[1][2 * (i + 1)].triTangent;

					tri_num = tri_num + 1;

				}
				temp_normal = temp_normal + sides[1][2 * i].triNormal;
				temp_tangent = temp_tangent + sides[1][2 * i].triTangent;
				temp_normal = temp_normal + sides[1][(2 * i) + 1].triNormal;
				temp_tangent = temp_tangent + sides[1][(2 * i) + 1].triTangent;
				tri_num = tri_num + 2;
			}


			temp_normal = temp_normal + drop[i][j].upperLeft.triNormal;
			temp_tangent = temp_tangent + drop[i][j].upperLeft.triTangent;
			tri_num = tri_num + 1;
			temp_normal = temp_normal / tri_num;
			temp_tangent = temp_tangent / tri_num;

			drop[i][j].lowerRight.normal[1] = temp_normal;
			drop[i][j].lowerRight.tangent[1] = temp_tangent;

		}
	}

	for (int i = 0; i < sides[0].size(); i=i+2)
	{
		temp_normal = Vector3(0.0f);
		temp_tangent = Vector3(0.0f);
		tri_num = 0;
		//dolny lewy
		if (i != 0)
		{
			temp_normal = temp_normal + sides[0][i - 1].triNormal;
			temp_tangent = temp_tangent + sides[0][i - 1].triTangent;
			tri_num = tri_num + 1;
		}
		temp_normal = temp_normal + sides[0][i].triNormal;
		temp_tangent = temp_tangent + sides[0][i].triTangent;
		temp_normal = temp_normal + sides[0][i + 1].triNormal;
		temp_tangent = temp_tangent + sides[0][i + 1].triTangent;
		tri_num = tri_num + 2;

		temp_normal = temp_normal / tri_num;
		temp_tangent = temp_tangent / tri_num;
		sides[0][i].normal[2] = temp_normal;
		sides[0][i].tangent[2] = temp_tangent;
		sides[0][i + 1].normal[0] = temp_normal;
		sides[0][i + 1].tangent[0] = temp_tangent;

		

		temp_normal = Vector3(0.0f);
		temp_tangent = Vector3(0.0f);
		tri_num = 0;
		//dolny prawy
		if (i != sides[0].size() -2)
		{
			temp_normal = temp_normal + sides[0][i +2].triNormal;
			temp_tangent = temp_tangent + sides[0][i +2].triTangent;
			temp_normal = temp_normal + sides[0][i+3].triNormal;
			temp_tangent = temp_tangent + sides[0][i+3].triTangent;
			tri_num = tri_num + 2;
		}
	
		temp_normal = temp_normal + sides[0][i + 1].triNormal;
		temp_tangent = temp_tangent + sides[0][i + 1].triTangent;
		tri_num = tri_num + 1;

		temp_normal = temp_normal / tri_num;
		temp_tangent = temp_tangent / tri_num;
		sides[0][i+1].normal[2] = temp_normal;
		sides[0][i+1].tangent[2] = temp_tangent;

		//pozostale
		sides[0][i].normal[0] = drop[i / 2].front().upperLeft.normal[0];
		sides[0][i].tangent[0] = drop[i / 2].front().upperLeft.tangent[0];
		sides[0][i].normal[1] = drop[i / 2].front().upperLeft.normal[2];
		sides[0][i].tangent[1] = drop[i / 2].front().upperLeft.tangent[2];
		sides[0][i + 1].normal[1] = drop[i / 2].front().upperLeft.normal[2];
		sides[0][i + 1].tangent[1] = drop[i / 2].front().upperLeft.tangent[2];
	


	}

	for (int i = 0; i < sides[0].size(); i=i+2)
	{
		temp_normal = Vector3(0.0f);
		temp_tangent = Vector3(0.0f);
		tri_num = 0;
		//dolny prawy
		if (i != 0)
		{
			temp_normal = temp_normal + sides[1][i - 1].triNormal;
			temp_tangent = temp_tangent + sides[1][i - 1].triTangent;
			tri_num = tri_num + 1;
		}
		temp_normal = temp_normal + sides[1][i].triNormal;
		temp_tangent = temp_tangent + sides[1][i].triTangent;
		temp_normal = temp_normal + sides[1][i + 1].triNormal;
		temp_tangent = temp_tangent + sides[1][i + 1].triTangent;
		tri_num = tri_num + 2;

		temp_normal = temp_normal / tri_num;
		temp_tangent = temp_tangent / tri_num;
		sides[1][i].normal[2] = temp_normal;
		sides[1][i].tangent[2] = temp_tangent;
		sides[1][i + 1].normal[2] = temp_normal;
		sides[1][i + 1].tangent[2] = temp_tangent;



		temp_normal = Vector3(0.0f);
		temp_tangent = Vector3(0.0f);
		tri_num = 0;
		//dolny lewy
		if (i != sides[0].size() - 2)
		{
			temp_normal = temp_normal + sides[1][i + 2].triNormal;
			temp_tangent = temp_tangent + sides[1][i + 2].triTangent;
			temp_normal = temp_normal + sides[1][i + 3].triNormal;
			temp_tangent = temp_tangent + sides[1][i + 3].triTangent;
			tri_num = tri_num + 2;
		}

		temp_normal = temp_normal + sides[1][i + 1].triNormal;
		temp_tangent = temp_tangent + sides[1][i + 1].triTangent;
		tri_num = tri_num + 1;

		temp_normal = temp_normal / tri_num;
		temp_tangent = temp_tangent / tri_num;
		sides[1][i + 1].normal[0] = temp_normal;
		sides[1][i + 1].tangent[0] = temp_tangent;

		//pozostale
		sides[1][i].normal[1] = drop[i/2].back().upperLeft.normal[1];
		sides[1][i].tangent[1] = drop[i / 2].back().upperLeft.tangent[1];
		sides[1][i].normal[0] = drop[i / 2].back().lowerRight.normal[1];
		sides[1][i].tangent[0] = drop[i / 2].back().lowerRight.tangent[1];
		sides[1][i + 1].normal[1] = drop[i / 2].back().lowerRight.normal[1];
		sides[1][i + 1].tangent[1] = drop[i / 2].back().lowerRight.tangent[1];



	}
}

void shapes::quadsToVertex(std::vector<std::vector<quad>> input, std::vector<VertexPositionNormalTextureTangent>& output)
{
	for (int i = 0; i < input.size(); i++)
	{
		for (int j = 0; j < input[0].size(); j++)
		{
			output.push_back(VertexPositionNormalTextureTangent(input[i][j].upperLeft.vertex[0], input[i][j].upperLeft.normal[0], input[i][j].upperLeft.tex[0], input[i][j].upperLeft.tangent[0], input[i][j].upperLeft.foam[0]));
			output.push_back(VertexPositionNormalTextureTangent(input[i][j].upperLeft.vertex[1], input[i][j].upperLeft.normal[1], input[i][j].upperLeft.tex[1], input[i][j].upperLeft.tangent[1], input[i][j].upperLeft.foam[1]));
			output.push_back(VertexPositionNormalTextureTangent(input[i][j].upperLeft.vertex[2], input[i][j].upperLeft.normal[2], input[i][j].upperLeft.tex[2], input[i][j].upperLeft.tangent[2], input[i][j].upperLeft.foam[2]));

			output.push_back(VertexPositionNormalTextureTangent(input[i][j].lowerRight.vertex[0], input[i][j].lowerRight.normal[0], input[i][j].lowerRight.tex[0], input[i][j].lowerRight.tangent[0], input[i][j].lowerRight.foam[0]));
			output.push_back(VertexPositionNormalTextureTangent(input[i][j].lowerRight.vertex[1], input[i][j].lowerRight.normal[1], input[i][j].lowerRight.tex[1], input[i][j].lowerRight.tangent[1], input[i][j].lowerRight.foam[1]));
			output.push_back(VertexPositionNormalTextureTangent(input[i][j].lowerRight.vertex[2], input[i][j].lowerRight.normal[2], input[i][j].lowerRight.tex[2], input[i][j].lowerRight.tangent[2], input[i][j].lowerRight.foam[2]));
		}
	}

}

void shapes::trianglesToVertex(std::vector<std::vector<triangle>> input, std::vector<VertexPositionNormalTextureTangent>& output)
{
	for (int i = 0; i < input.size(); i++)
	{
		for (int j = 0; j < input[0].size(); j++)
		{
			output.push_back(VertexPositionNormalTextureTangent(input[i][j].vertex[0], input[i][j].normal[0], input[i][j].tex[0], input[i][j].tangent[0], input[i][j].foam[0]));
			output.push_back(VertexPositionNormalTextureTangent(input[i][j].vertex[1], input[i][j].normal[1], input[i][j].tex[1], input[i][j].tangent[1], input[i][j].foam[1]));
			output.push_back(VertexPositionNormalTextureTangent(input[i][j].vertex[2], input[i][j].normal[2], input[i][j].tex[2], input[i][j].tangent[2], input[i][j].foam[2]));
		}
	}
}


