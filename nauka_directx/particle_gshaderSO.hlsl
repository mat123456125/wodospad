struct Vertex
{
	float3 PosW			: POSITION;
	float3 VelW			: VELOCITY;
	float2 SizeW		: SIZE;
	float Age			: AGE;
	uint Type			: TYPE;
	uint Texture		: TEX; // indeks w tablicy tekstur
	uint Rotation		: ROTATION;  //obroty o wielokrotnosc 90 stopni
};

#include "particle_cbuffer.hlsl"

Texture1D gRandomTex;
SamplerState samLinear;

float3 RandUnitVec3(float offset)
{
	// Use game time plus offset to sample random texture.
	float u = (gGameTime + offset);
	// coordinates in [-1,1]
	float3 v = gRandomTex.SampleLevel(samLinear, u, 0).xyz;
	// project onto unit sphere
	return normalize(v);
}

float3 RandUnsignedFloat(float offset)
{
	// Use game time plus offset to sample random texture.
	float u = (gGameTime + offset);
	// coordinates in [-1,1]
	float v = gRandomTex.SampleLevel(samLinear, u, 0).x;
	v = abs(v);
	return v;
}





[maxvertexcount(2)]
void main(point Vertex gin[1],
	inout PointStream<Vertex> ptStream)
{
	gin[0].Age += gTimeStep;
	
	if (gin[0].Type == 0) //emiter
	{
		
		//float nPTime = 0.55f + 0.3f*vRandom.x;
		// time to emit a new particle
		if (gin[0].Age > 0.035f)//nPTime
		{
			float off = gin[0].PosW.x * 0.01;
			float3 vRandom = RandUnitVec3(off);
			float rand = RandUnsignedFloat(off);
			float rand2 = RandUnsignedFloat(off +0.3f);
			
			vRandom.x *= 0.5f;
			vRandom.z *= 0.5f;
			Vertex p;
			p.PosW =  gin[0].PosW+ 0.12f*vRandom;
			p.VelW = gInitialVel + 0.2f*vRandom;//0.2
			
			p.SizeW = float2(0.35f, 0.35f);
			/*if(rand<0.2f)
				p.SizeW = float2(0.25f, 0.25f);*/
			p.Age = 0.0f;
			p.Type = 1;
			p.Texture = 1+rand*4;
			p.Rotation = rand2 * 4;
			gin[0].Age = 0.0f; // reset the time to emit
			ptStream.Append(gin[0]);
			ptStream.Append(p);
			
			
			
			
		}	
		else
			ptStream.Append(gin[0]);// always keep emitters
		
	}
	else if (gin[0].Type == 1) //woda spadajaca
	{
		// Specify conditions to keep particle; 
		if (gin[0].Age <= 30.50f)
		{
			if (gin[0].PosW.y < -0.2)
			{	
				float off = gin[0].PosW.x * 0.01;
				float rand = RandUnsignedFloat(off);
				float rand2 = RandUnsignedFloat(off + 0.3f);
				float3 vRandom2 = RandUnitVec3(off);
				Vertex p;
				
					gin[0].PosW = gin[0].PosW + float3(0,0.1,0);
					gin[0].VelW = (float3(0.0f, 0.75f, 0.75f) + 0.4f*vRandom2); // mozna pomyslec 0.5
					gin[0].SizeW = float2(0.3f, 0.3f);
					gin[0].Texture = 1 + rand2 * 4;
					gin[0].Rotation = rand2 * 4;
					gin[0].Age = 0.0f;
					gin[0].Type = 2;   // odbicie strumienia wody
				
					p.PosW = gin[0].PosW + float3(0, 0.1, 0);
					p.VelW = float3(0.0f, 0.5f, 0.8f) + 0.2f*vRandom2;
					p.SizeW = float2(0.4f, 0.4f);
					p.Age = 0.0f;
					p.Texture = 1 + rand2 * 4;
					p.Type = 3; // piana plaska
					p.Rotation = gin[0].Rotation;
					ptStream.Append(p);
				
			}

				ptStream.Append(gin[0]);
		}
	}
	else if (gin[0].Type == 2)
	{
		if (gin[0].Age <= 1.2f)
			ptStream.Append(gin[0]);

	}
	else
	{
		// Specify conditions to keep particle; this may vary
		// from system to system.
		if (gin[0].Age <= 2.0f)
			ptStream.Append(gin[0]);
	}
}