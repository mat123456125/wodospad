struct GeoOut
{
	float4 PosH : SV_Position;
	float4 Color : COLOR;
	float2 Tex : TEXCOORD;
	uint Texture	: TEX;
};

Texture2DArray gTexArray;
SamplerState samLinear;

struct PsOut
{
	float4 accum : SV_TARGET0;
	float revealage : SV_TARGET1;
	
};

PsOut main(GeoOut pin) 
{
	PsOut pout;
	float4 texColor = gTexArray.Sample(samLinear, float3(pin.Tex,pin.Texture))*pin.Color;
	float temp = max(texColor.r, texColor.g);
	temp = max(temp, texColor.b);
	temp = min(1.0, temp  * texColor.a);
	//float weight =	max(temp, texColor.a) * clamp(0.03 / (1e-5 + pow(pin.PosH.z / 50, 4.0)), 1e-2, 3e2);  //stara wersja

	float weight = max(temp, texColor.a) * clamp((3e2 * pow(pin.PosH.z , 3.0)), 1e-2, 3e2);
	pout.accum = float4(texColor.rgb * texColor.a, texColor.a) * weight;
	//pout.accum = texColor;
	pout.revealage = texColor.a;
	return pout;
}