#include "randomHelper.h"

using namespace std;

randomHelper::randomHelper()
{
	random_device rd;
	generator = mt19937(rd());
}


randomHelper::~randomHelper()
{
}

float randomHelper::randf()
{
	uniform_real_distribution<float> distribution(0.0f, 1.0f);
	return distribution(generator);

}

float randomHelper::randf(float a, float b)
{
	uniform_real_distribution<float> distribution(a, b);
	return distribution(generator);
	
}

DirectX::SimpleMath::Vector3 randomHelper::randUnitVector3() //losowy vector o dl 1
{
	float theta = randf(0, 2 * 3.14159);
	float z = randf(-1.0f, 1.0f);
	float x = sqrtf(1.0f - z*z)*cosf(theta);
	float y = sqrtf(1.0f - z*z)*sinf(theta);
	return DirectX::SimpleMath::Vector3(x,y,z);
}

