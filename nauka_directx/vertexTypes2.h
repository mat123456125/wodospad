#pragma once
#include <SimpleMath.h>

struct VertexPositionNormalTextureTangent
{
	VertexPositionNormalTextureTangent(DirectX::SimpleMath::Vector3 const& position, DirectX::SimpleMath::Vector3 const& normal,
		DirectX::SimpleMath::Vector2 const& textureCoordinate, DirectX::SimpleMath::Vector3 tangent, DirectX::SimpleMath::Vector3 foam)
		: position(position),
		normal(normal),
		textureCoordinate(textureCoordinate),
		tangent(tangent),
		foam(foam)
	{ }
	VertexPositionNormalTextureTangent(DirectX::SimpleMath::Vector3 const& position, DirectX::SimpleMath::Vector3 const& normal,
		DirectX::SimpleMath::Vector2 const& textureCoordinate, DirectX::SimpleMath::Vector3 tangent)
		: position(position),
		normal(normal),
		textureCoordinate(textureCoordinate),
		tangent(tangent),
		foam(DirectX::SimpleMath::Vector3(0,0,0))
	{ }
	VertexPositionNormalTextureTangent()
		: position(DirectX::SimpleMath::Vector3(0, 0, 0)),
		normal(DirectX::SimpleMath::Vector3(0, 0, 0)),
		textureCoordinate(DirectX::SimpleMath::Vector2(0, 0)),
		tangent(DirectX::SimpleMath::Vector3(0, 0, 0)),
		foam(DirectX::SimpleMath::Vector3(0, 0, 0))
	{ }

	DirectX::SimpleMath::Vector3 position;
	DirectX::SimpleMath::Vector3 normal;
	DirectX::SimpleMath::Vector2 textureCoordinate;
	DirectX::SimpleMath::Vector3 tangent;
	DirectX::SimpleMath::Vector3 foam; // tylko x ma znaczenie narazie
};