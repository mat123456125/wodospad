#include "camera.h"


using namespace DirectX;
using namespace SimpleMath;

camera::camera()
{
	camPos = Vector3(0, 0, 0);
	angleX =  0.45; //wokol osi x
	angleY = 0;// 0.89 *3.14; //osi Y
	zoom = -4.0f;//5
}


camera::~camera()
{
}

DirectX::SimpleMath::Matrix camera::computeCamera()
{
	auto state = Mouse::Get().GetState();
	tracker.Update(state);

	zoom = -4 + 0.003f * state.scrollWheelValue; // zoom


	if (tracker.leftButton == Mouse::ButtonStateTracker::ButtonState::PRESSED)
	{
		Mouse::Get().SetMode(Mouse::MODE_RELATIVE);
	}
	else if (tracker.leftButton == Mouse::ButtonStateTracker::ButtonState::RELEASED)
	{
		Mouse::Get().SetMode(Mouse::MODE_ABSOLUTE);
	}
	

	if (state.positionMode == Mouse::MODE_RELATIVE)
	{
		angleX = angleX + state.y * 0.002f;
		angleY = angleY + state.x * 0.002f;
		
	}

	Matrix result = Matrix::CreateTranslation(-camPos) * Matrix::CreateRotationY(angleY) * Matrix::CreateRotationX(angleX) * Matrix::CreateTranslation(0,0, zoom);
	return result;
}
