#include "meshLoader.h"

using namespace DirectX;

std::vector<VertexPositionNormalTexture> meshLoader::loadVPNT(std::string file)
{
	
	const aiScene* scene = loadScene(file);
	
	
	if (!scene )
	{
		//TODO error error
		return std::vector<VertexPositionNormalTexture>();
	}

	std::vector<VertexPositionNormalTexture> temp;
	for (int j = 0; j < scene->mNumMeshes; j++)
	{

		for (int i = 0; i < scene->mMeshes[j]->mNumVertices; i++)
		{
			temp.push_back(VertexPositionNormalTexture(
				SimpleMath::Vector3(scene->mMeshes[j]->mVertices[i].x, scene->mMeshes[j]->mVertices[i].y, scene->mMeshes[j]->mVertices[i].z),
				SimpleMath::Vector3(scene->mMeshes[j]->mNormals[i].x, scene->mMeshes[j]->mNormals[i].y, scene->mMeshes[j]->mNormals[i].z),
				SimpleMath::Vector2(scene->mMeshes[j]->mTextureCoords[0][i].x, scene->mMeshes[j]->mTextureCoords[0][i].y)));

		}
	}
	return temp;
}

std::vector<VertexPositionNormalTextureTangent> meshLoader::loadVPNTT(std::string file) //TODO do poprawy
{
	Assimp::Importer importer; //TODO naprawic musi istniec obiekt tu
	// warto jedne obiekt na program, klasa juz nie static?

	const aiScene* scene = importer.ReadFile(file.c_str(),
		aiProcess_CalcTangentSpace | aiProcess_Triangulate |
		aiProcess_MakeLeftHanded);
	//const aiScene* scene = loadScene(file);


	if (!scene)
	{
		//TODO error error
		return std::vector<VertexPositionNormalTextureTangent>();
	}

	std::vector<VertexPositionNormalTextureTangent> temp;
	for (int j = 0; j < scene->mNumMeshes; j++)
	{
		
		for (int i = 0; i < scene->mMeshes[j]->mNumVertices; i++)
		{
			temp.push_back(VertexPositionNormalTextureTangent(
				SimpleMath::Vector3(scene->mMeshes[j]->mVertices[i].x, scene->mMeshes[j]->mVertices[i].y, scene->mMeshes[j]->mVertices[i].z),
				SimpleMath::Vector3(scene->mMeshes[j]->mNormals[i].x, scene->mMeshes[j]->mNormals[i].y, scene->mMeshes[j]->mNormals[i].z),
				SimpleMath::Vector2(scene->mMeshes[j]->mTextureCoords[0][i].x, scene->mMeshes[j]->mTextureCoords[0][i].y),
				SimpleMath::Vector3(scene->mMeshes[j]->mTangents[i].x, scene->mMeshes[j]->mTangents[i].y, scene->mMeshes[j]->mTangents[i].z)));

		}
	}
	return temp;
}

const aiScene * meshLoader::loadScene(std::string file)
{
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(file.c_str(),
		aiProcess_CalcTangentSpace| aiProcess_Triangulate |
		aiProcess_MakeLeftHanded);

	if (!scene->HasMaterials())
	{
		//TODO wiecej sprawdzen error error
		return nullptr;
	}
	return scene;

}
