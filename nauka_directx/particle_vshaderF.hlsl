struct Vertex
{
	float3 Pos : POSITION;
};
struct VertexOut
{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD;
};

VertexOut VS(Vertex vin)
{
	VertexOut vout;
	vout.Pos = float4(vin.Pos,1.0);
	vout.Tex = vin.Pos.xy;
	vout.Tex.y = -1 * vout.Tex.y;
	vout.Tex = (vout.Tex + 1) / 2;
	return vout;
}