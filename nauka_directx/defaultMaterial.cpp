#include "defaultMaterial.h"
#include "vshader.h"
#include "pshader.h"



defaultMaterial::defaultMaterial(programData* pd)
{
	prData = pd;
	gDevice = prData->gDevice;
	HRESULT res;
	res = gDevice->dxDevice->CreateVertexShader(vshader, sizeof(vshader), nullptr, &m_vshader);

	res = gDevice->dxDevice->CreatePixelShader(pshader, sizeof(pshader), nullptr, &m_pshader);
}


defaultMaterial::~defaultMaterial()
{
	m_pshader->Release();
	m_vshader->Release();
}

void defaultMaterial::set()
{
	gDevice->dxContext->VSSetShader(m_vshader, 0, 0);
	gDevice->dxContext->PSSetShader(m_pshader, 0, 0);
}
