#include "particleSystem.h"

#include "particle_vshader.h"
#include "particle_vshaderSO.h"
#include "particle_pshader.h"
#include "particle_gshader.h"
#include "particle_gshaderSO.h"
#include "particle_pshaderF.h"
#include "particle_vshaderF.h"



using namespace DirectX;
using namespace SimpleMath;

particleSystem::particleSystem(programData* pr, UINT max_vert, std::vector<DirectX::SimpleMath::Vector3>& emitPos)
{
	prData = pr;
	gDevice = prData->gDevice;
	max_vertices = max_vert;
	num_emiters = emitPos.size();
	if (num_emiters == 0)
	{
		throw ;
	}

	mFirstRun = true;
	mGameTime = 0;
	mAge = 0;
	mInitialVel = Vector3(0.0f, -0.2f, 0.3f);
	mAccel = Vector3(0, -1, 0);

	CreateBState();
	CreateInputLayout();
	CreateVertexBuffers(emitPos);
	CreateShaders();
	mRandomTex = createRandomTexture();
	m_constBufferData = new constBufferData();

}


particleSystem::~particleSystem()
{
}

void particleSystem::Reset()
{
	mFirstRun = true;
	mAge = 0.0f;
	
}

void particleSystem::Update(std::vector<DirectX::SimpleMath::Vector3>& emitPos)
{
	num_emiters = emitPos.size();
	if (num_emiters == 0)
	{
		throw;
	}

	mFirstRun = true;
	UpdateVertexBuffers(emitPos);
}

void particleSystem::Draw()
{
	mGameTime = mGameTime + prData->deltaTime;
	
	m_constBufferData->gAccel = mAccel;
	m_constBufferData->gInitialVel = mInitialVel;
	m_constBufferData->gEyePosW = prData->m_PSDataFrame.gEyePosW;
	m_constBufferData->gGameTime = mGameTime;
	m_constBufferData->gTimeStep = prData->deltaTime;
	m_constBufferData->gViewProj = prData->viewProjMatrix;
	m_constBufferData->collPlane = prData->collisionPlane;

	setStateSO();

	if (mFirstRun)
	{
		gDevice->dxContext->Draw(num_emiters, 0);
		mFirstRun = false;
	}
	else
	{
		gDevice->dxContext->DrawAuto();
	}

	setState();

	gDevice->dxContext->DrawAuto();
	setStateFinal();
	gDevice->dxContext->Draw(6, 0);
	
}

ID3D11ShaderResourceView * particleSystem::createRandomTexture()
{
	Vector4 randomValues[1024];
	randomHelper random;
	for (int i = 0; i < 1024; ++i)
	{
		randomValues[i].x = random.randf(-1.0f, 1.0f);
		randomValues[i].y = random.randf(-1.0f, 1.0f);
		randomValues[i].z = random.randf(-1.0f, 1.0f);
		randomValues[i].w = random.randf(-1.0f, 1.0f);
	}
	D3D11_SUBRESOURCE_DATA initData;
	initData.pSysMem = randomValues;
	initData.SysMemPitch = 1024 * sizeof(XMFLOAT4);
	initData.SysMemSlicePitch = 0;

	D3D11_TEXTURE1D_DESC texDesc;
	texDesc.Width = 1024;
	texDesc.MipLevels = 1;
	texDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	texDesc.Usage = D3D11_USAGE_IMMUTABLE;
	texDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	texDesc.CPUAccessFlags = 0;
	texDesc.MiscFlags = 0;
	texDesc.ArraySize = 1;
	ID3D11Texture1D* randomTex = 0;
	gDevice->dxDevice->CreateTexture1D(&texDesc, &initData, &randomTex);

	D3D11_SHADER_RESOURCE_VIEW_DESC viewDesc;
	viewDesc.Format = texDesc.Format;
	viewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE1D;
	viewDesc.Texture1D.MipLevels = texDesc.MipLevels;
	viewDesc.Texture1D.MostDetailedMip = 0;
	ID3D11ShaderResourceView* randomTexSRV = 0;
	gDevice->dxDevice->CreateShaderResourceView(randomTex, &viewDesc, &randomTexSRV);

	randomTex->Release();
	return randomTexSRV;
}

void particleSystem::setState()
{
	gDevice->dxContext->VSSetShader(m_vshader, 0, 0);
	gDevice->dxContext->GSSetShader(m_gshader, 0, 0);
	gDevice->dxContext->PSSetShader(m_pshader, 0, 0);

	gDevice->dxContext->VSSetConstantBuffers(0, 1, &constBuffer);
	gDevice->dxContext->GSSetConstantBuffers(0, 1, &constBuffer);

	gDevice->readonlyDepth();
	
	
	ID3D11Buffer* bufferArray[1] = { 0 };
	UINT stride = sizeof(Particle);
	UINT offset = 0;
	gDevice->dxContext->SOSetTargets(1, bufferArray, &offset);

	std::swap(vertexBuffer1, vertexBuffer2); //podmiana buforow
	
	gDevice->dxContext->IASetVertexBuffers(0, 1, &vertexBuffer1, &stride, &offset);

	gDevice->dxContext->PSSetShaderResources(0, 1, &mTexArray);

	float blendFactors[] = { 0.0f, 0.0f, 0.0f, 0.0f };
	gDevice->dxContext->OMSetBlendState(particleBlendState, blendFactors, 0xffffffff);

	gDevice->setSpecialRenderTarg();
	float zero[4] = { 0,0,0,0 };
	float one[4] = { 1,1,1,1 };
	gDevice->dxContext->ClearRenderTargetView(gDevice->blendingRenderTarget[0], zero);
	gDevice->dxContext->ClearRenderTargetView(gDevice->blendingRenderTarget[1], one);
}

void particleSystem::setStateSO()
{
	gDevice->dxContext->VSSetShader(m_vshaderSO, 0, 0);
	gDevice->dxContext->GSSetShader(m_gshaderSO, 0, 0);
	gDevice->dxContext->PSSetShader(NULL, 0, 0);
	
	

	D3D11_MAPPED_SUBRESOURCE mappedResource;
	

	gDevice->dxContext->Map(constBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	memcpy(mappedResource.pData, m_constBufferData, sizeof(constBufferData));
	gDevice->dxContext->Unmap(constBuffer, 0);

	gDevice->dxContext->VSSetConstantBuffers(0, 1, &constBuffer);	
	gDevice->dxContext->GSSetConstantBuffers(0, 1, &constBuffer);
	gDevice->dxContext->GSSetSamplers(0, 1, &samplerState);
	gDevice->dxContext->GSSetShaderResources(0, 1, &mRandomTex);


	

	gDevice->dxContext->IASetInputLayout(inLayout);
	gDevice->dxContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

	UINT stride = sizeof(Particle);
	UINT offset = 0; 

	if (mFirstRun)
		gDevice->dxContext->IASetVertexBuffers(0, 1, &mInitVB, &stride, &offset);
	else
		gDevice->dxContext->IASetVertexBuffers(0, 1, &vertexBuffer1, &stride, &offset);

	gDevice->dxContext->SOSetTargets(1, &vertexBuffer2, &offset);
	gDevice->disableDepth();

}

void particleSystem::setStateFinal()
{
	gDevice->setDefaultRenderTarg();

	float blendFactors[] = { 0.0f, 0.0f, 0.0f, 0.0f };
	gDevice->dxContext->OMSetBlendState(normalBlendState, blendFactors, 0xffffffff);
	gDevice->dxContext->VSSetShader(m_vshaderF, 0, 0);
	gDevice->dxContext->GSSetShader(NULL, 0, 0);
	gDevice->dxContext->PSSetShader(m_pshaderF, 0, 0);
	gDevice->dxContext->PSSetShaderResources(0, 2, gDevice->blendingShaderResource);
	gDevice->dxContext->PSSetSamplers(0, 1, &samplerState);

	gDevice->dxContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	gDevice->dxContext->IASetInputLayout(inLayout2);
	UINT stride = sizeof(DirectX::SimpleMath::Vector3);
	UINT offset = 0;
	gDevice->dxContext->IASetVertexBuffers(0, 1, &screenRectVB, &stride, &offset);
	gDevice->disableDepth();

	
}

void particleSystem::CreateBState()
{
	D3D11_BLEND_DESC particleBlendDesc = { 0 };
	particleBlendDesc.AlphaToCoverageEnable = false;
	particleBlendDesc.IndependentBlendEnable = true;
	particleBlendDesc.RenderTarget[0].BlendEnable = true;
	particleBlendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	particleBlendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	particleBlendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
	particleBlendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	particleBlendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	particleBlendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	particleBlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

	/*particleBlendDesc.RenderTarget[0].BlendEnable = true;
	particleBlendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	particleBlendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	particleBlendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	particleBlendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	particleBlendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	particleBlendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	particleBlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;*/



	particleBlendDesc.RenderTarget[1].BlendEnable = true;
	particleBlendDesc.RenderTarget[1].BlendOp = D3D11_BLEND_OP_ADD;
	particleBlendDesc.RenderTarget[1].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	particleBlendDesc.RenderTarget[1].DestBlend = D3D11_BLEND_INV_SRC_COLOR;
	particleBlendDesc.RenderTarget[1].SrcBlend = D3D11_BLEND_ZERO;
	particleBlendDesc.RenderTarget[1].SrcBlendAlpha = D3D11_BLEND_ZERO;
	particleBlendDesc.RenderTarget[1].DestBlendAlpha = D3D11_BLEND_INV_SRC_ALPHA;
	particleBlendDesc.RenderTarget[1].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;


	gDevice->dxDevice->CreateBlendState(&particleBlendDesc, &particleBlendState);

	particleBlendDesc.IndependentBlendEnable = false;
	particleBlendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_SRC_ALPHA;
	particleBlendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_INV_SRC_ALPHA;
	particleBlendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	particleBlendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;

	/*particleBlendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	particleBlendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	particleBlendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	particleBlendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;*/

	gDevice->dxDevice->CreateBlendState(&particleBlendDesc, &normalBlendState);

	

	D3D11_SAMPLER_DESC samplerDesc;
	ZeroMemory(&samplerDesc, sizeof(D3D11_SAMPLER_DESC));
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	

	gDevice->dxDevice->CreateSamplerState(&samplerDesc, &samplerState);


}

void particleSystem::CreateVertexBuffers(std::vector<DirectX::SimpleMath::Vector3>& emitPos)
{

	Particle* startParticles = new Particle[num_emiters];
	for (int i = 0; i < num_emiters; i++)
	{
		startParticles[i].Age = 0.0f;
		startParticles[i].Type = 0;
		startParticles[i].Pos = emitPos[i];
		startParticles[i].Vel = Vector3(i,i,i);
	}

	
	
	
	D3D11_BUFFER_DESC vertexbd;
	vertexbd.Usage = D3D11_USAGE_DEFAULT;
	vertexbd.ByteWidth = sizeof(Particle) * max_vertices;
	vertexbd.BindFlags = D3D11_BIND_VERTEX_BUFFER | D3D11_BIND_STREAM_OUTPUT;
	vertexbd.CPUAccessFlags = 0;
	vertexbd.MiscFlags = 0;

	gDevice->dxDevice->CreateBuffer(&vertexbd, 0, &vertexBuffer1);
	gDevice->dxDevice->CreateBuffer(&vertexbd, 0, &vertexBuffer2);

	vertexbd.ByteWidth = sizeof(Particle)* num_emiters;
	vertexbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	
	D3D11_SUBRESOURCE_DATA verData;
	verData.pSysMem = startParticles;
	gDevice->dxDevice->CreateBuffer(&vertexbd, &verData, &mInitVB);


	DirectX::SimpleMath::Vector3 Rect[6] =
	{
		DirectX::SimpleMath::Vector3(-1,1,0),
		DirectX::SimpleMath::Vector3( 1,1,0),
		DirectX::SimpleMath::Vector3(-1,-1,0),
		DirectX::SimpleMath::Vector3(-1,-1,0),
		DirectX::SimpleMath::Vector3(1,1,0),
		DirectX::SimpleMath::Vector3(1,-1,0)
	};
	vertexbd.ByteWidth = sizeof(DirectX::SimpleMath::Vector3)* 6;
	vertexbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	verData.pSysMem = Rect;
	gDevice->dxDevice->CreateBuffer(&vertexbd, &verData, &screenRectVB);
	

	vertexbd.ByteWidth = sizeof(constBufferData);
	vertexbd.Usage = D3D11_USAGE_DYNAMIC;
	vertexbd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	vertexbd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	vertexbd.MiscFlags = 0;
	vertexbd.StructureByteStride = 0;

	gDevice->dxDevice->CreateBuffer(&vertexbd, 0, &constBuffer);
}

void particleSystem::UpdateVertexBuffers(std::vector<DirectX::SimpleMath::Vector3>& emitPos)
{
	mInitVB->Release();

	Particle* startParticles = new Particle[num_emiters];
	for (int i = 0; i < num_emiters; i++)
	{
		startParticles[i].Age = 0.0f;
		startParticles[i].Type = 0;
		startParticles[i].Pos = emitPos[i];
		startParticles[i].Vel = Vector3(i, i, i);
	}

	D3D11_BUFFER_DESC vertexbd;
	vertexbd.Usage = D3D11_USAGE_DEFAULT;
	vertexbd.ByteWidth = sizeof(Particle)* num_emiters;
	vertexbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexbd.CPUAccessFlags = 0;
	vertexbd.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA verData;
	verData.pSysMem = startParticles;
	gDevice->dxDevice->CreateBuffer(&vertexbd, &verData, &mInitVB);
}

void particleSystem::CreateShaders()
{
	D3D11_SO_DECLARATION_ENTRY soDecl[] =
	{
		//position, semantic name, semantic index, start component, component count, output slot
		{ 0,"POSITION", 0, 0, 3, 0 }, // output first 3 components of SPEED
		{ 0, "VELOCITY", 0, 0, 3, 0 }, // output first 3 components of "POSITION"
		{ 0, "SIZE", 0, 0, 2, 0 }, // output first 2 components of SIZE
		{ 0, "AGE", 0, 0, 1, 0 }, // output AGE
		{ 0, "TYPE", 0, 0, 1, 0 }, // output TYPE
		{ 0, "TEX", 0, 0, 1, 0 }, 
		{ 0, "ROTATION", 0, 0, 1, 0 }, 

	};

	UINT strides[] = { sizeof(Particle) };
	HRESULT res;
	res = gDevice->dxDevice->CreateGeometryShaderWithStreamOutput(particle_gshaderSO, sizeof(particle_gshaderSO), soDecl, 7, strides, 1, 0, NULL, &m_gshaderSO);
	res = gDevice->dxDevice->CreateGeometryShader(particle_gshader, sizeof(particle_gshader), NULL, &m_gshader);
	res = gDevice->dxDevice->CreateVertexShader(particle_vshader, sizeof(particle_vshader), NULL, &m_vshader);
	res = gDevice->dxDevice->CreateVertexShader(particle_vshaderSO, sizeof(particle_vshaderSO), NULL, &m_vshaderSO);
	res = gDevice->dxDevice->CreatePixelShader(particle_pshader, sizeof(particle_pshader), NULL, &m_pshader);
	res = gDevice->dxDevice->CreateVertexShader(particle_vshaderF, sizeof(particle_vshaderF), NULL, &m_vshaderF);
	res = gDevice->dxDevice->CreatePixelShader(particle_pshaderF, sizeof(particle_pshaderF), NULL, &m_pshaderF);
}
void particleSystem::CreateInputLayout()
{
	D3D11_INPUT_ELEMENT_DESC InputElements[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT,    0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "VELOCITY",      0, DXGI_FORMAT_R32G32B32_FLOAT,    0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "SIZE",    0, DXGI_FORMAT_R32G32_FLOAT,       0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "AGE",    0, DXGI_FORMAT_R32_FLOAT,       0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TYPE",    0, DXGI_FORMAT_R32_UINT,       0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEX",    0, DXGI_FORMAT_R32_UINT,       0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "ROTATION",    0, DXGI_FORMAT_R32_UINT,       0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	HRESULT res;
	res = gDevice->dxDevice->CreateInputLayout(InputElements, 7, particle_vshader, sizeof(particle_vshader), &inLayout);
	InputElements[0] =	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT,    0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 };
	res = gDevice->dxDevice->CreateInputLayout(InputElements, 1, particle_vshaderF, sizeof(particle_vshaderF), &inLayout2);
	
}
