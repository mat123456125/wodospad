#include "waterSurface.h"
#include "water_pshader.h"
#include "water_vshader.h"

using namespace DirectX;

waterSurface::waterSurface(programData* pd)
{
	prData = pd;
	gDevice = prData->gDevice;
	
	gameTime = 0.0f;

	HRESULT res;

	res = gDevice->dxDevice->CreateVertexShader(water_vshader, sizeof(water_vshader), nullptr, &m_vshader);

	res = gDevice->dxDevice->CreatePixelShader(water_pshader, sizeof(water_pshader), nullptr, &m_pshader);

	
	loadTextures();

}


waterSurface::~waterSurface()
{	
	m_pshader->Release();
	m_vshader->Release();
	SResViewTex[0]->Release();
	SResViewTex[1]->Release();
	SResViewTex[2]->Release();
}

void waterSurface::set() /// TODO tu zaczac czy  robic ustawianie stanu tylk oczy rysowanie
{
	gDevice->dxContext->PSSetShaderResources(0, 4, SResViewTex);

	gDevice->dxContext->VSSetShader(m_vshader, 0, 0);
	gDevice->dxContext->PSSetShader(m_pshader, 0, 0);

	gameTime += prData->deltaTime;
	prData->m_PSDataObject.gameTime = gameTime;	


}


void waterSurface::loadTextures()
{
	HRESULT result;
	result = CreateWICTextureFromFile(gDevice->dxDevice, L"water_tex.jpg", nullptr, &SResViewTex[0]);
	result = CreateWICTextureFromFile(gDevice->dxDevice, L"woda_spad_normal2.png", nullptr, &SResViewTex[1]);
	result = CreateWICTextureFromFile(gDevice->dxDevice, L"normal1.jpg", nullptr, &SResViewTex[2]);
	result = CreateWICTextureFromFile(gDevice->dxDevice, L"normal2.jpg", nullptr, &SResViewTex[3]);
}

