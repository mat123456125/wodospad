#include "mesh.h"

using namespace DirectX;
using namespace SimpleMath;
using namespace std;




mesh::mesh(int sID, int len, programData* pd, meshMaterial* mat)
{
	startIndex = sID;
	size = len; 
	
	m_lighting.Ambient = Vector4(0.5f, 0.5f, 0.5f, 1.0f);
	m_lighting.Diffuse = Vector4(0.8f, 0.8f, 0.8f, 1.0f);
	m_lighting.Specular = Vector4(0.8f, 0.8f, 0.8f, 150.0f);

	worldMatrix = Matrix::Identity;
	prData = pd;
	m_material = mat;
	gDevice = prData->gDevice;
}


mesh::~mesh()
{
}

void mesh::draw()
{
	prData->m_VSData.finalMatrix =   worldMatrix * prData->viewProjMatrix;
	prData->m_VSData.worldMatrix = worldMatrix;
	prData->m_VSData.worldInvTransMatrix = worldMatrix.Invert().Transpose();

	prData->m_PSDataObject.gLighting = m_lighting;
	
	m_material->set();
	gDevice->dxContext->UpdateSubresource(prData->PSObjectConstBuffer, 0, NULL, &prData->m_PSDataObject, 0, 0);
	gDevice->dxContext->UpdateSubresource(prData->VSConstBuffer, 0, NULL, &prData->m_VSData, 0, 0);
	gDevice->dxContext->Draw(size, startIndex);
}
