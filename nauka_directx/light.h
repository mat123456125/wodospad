#pragma once
#include <SimpleMath.h>
#include <Windows.h>


class objectLighting
{
public:
	objectLighting() { ZeroMemory(this, sizeof(this)); }
	DirectX::SimpleMath::Vector4 Ambient;
	DirectX::SimpleMath::Vector4 Diffuse;
	DirectX::SimpleMath::Vector4 Specular;
	DirectX::SimpleMath::Vector4 Reflect;
};

class directionalLight
{
public:
	directionalLight() { ZeroMemory(this, sizeof(this)); }
	DirectX::SimpleMath::Vector4 Ambient;
	DirectX::SimpleMath::Vector4 Diffuse;
	DirectX::SimpleMath::Vector4 Specular;
	DirectX::SimpleMath::Vector3 Direction;
	float pad;

};

class pointLight
{
public:
	pointLight() { ZeroMemory(this, sizeof(this)); }
	DirectX::SimpleMath::Vector4 Ambient;
	DirectX::SimpleMath::Vector4 Diffuse;
	DirectX::SimpleMath::Vector4 Specular;
	DirectX::SimpleMath::Vector3 Position;
	float range;
	DirectX::SimpleMath::Vector3 Att;
	float pad;

};

class spotLight
{
public:
	spotLight() { ZeroMemory(this, sizeof(this)); }
	DirectX::SimpleMath::Vector4 Ambient;
	DirectX::SimpleMath::Vector4 Diffuse;
	DirectX::SimpleMath::Vector4 Specular;
	DirectX::SimpleMath::Vector3 Position;
	float range;
	DirectX::SimpleMath::Vector3 Direction;
	float spot;
	DirectX::SimpleMath::Vector3 Att;
	float pad;

};



//class light
//{
//public:
//	//static void prepare(PSDataFrame* data); //do poprawienia
//	
//};