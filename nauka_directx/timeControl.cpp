#include "timeControl.h"



timeControl::timeControl()
{
	__int64 countsPerSec;
	
	QueryPerformanceCounter((LARGE_INTEGER*)&currTime);
	QueryPerformanceFrequency((LARGE_INTEGER*)&countsPerSec);
	mSecondsPerCount = 1.0 / (double)countsPerSec;

	timeToSec = 0;
	fps = 0;
	temp_fps = 0;
}



double timeControl::frame() 
{
	__int64 newTime;
	QueryPerformanceCounter((LARGE_INTEGER*)&newTime);
	__int64 deltaTime = newTime - currTime;
	currTime = newTime;
	double deltaSecounds = deltaTime * mSecondsPerCount;

	timeToSec = timeToSec + deltaSecounds;
	temp_fps++;
	if (timeToSec > 0.5)
	{
		timeToSec = 0;
		fps = 2 * temp_fps;
		temp_fps = 0;
	}

	return deltaSecounds;
}
