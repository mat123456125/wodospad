#pragma once

#include "directxDevice.h"
#include "meshMaterial.h"
#include "light.h"
#include "shapes.h"
#include "vertexTypes2.h"
#include <WICTextureLoader.h>
#include <vector>
#include "programData.h"



struct VSBufferData
{
	DirectX::SimpleMath::Matrix finalMatrix;
	DirectX::SimpleMath::Matrix worldMatrix;
	DirectX::SimpleMath::Matrix worldInvTransMatrix;
	//TODO zmiany

};

struct PSBufferData
{
	float gameTime;
	DirectX::SimpleMath::Vector3 pad;
};

//do rysowania plynacej wody
class waterSurface :public meshMaterial
{
public:
	waterSurface(programData* pd);
	~waterSurface();
	virtual void set();

private:
	DirectxDevice* gDevice;
	programData* prData;
	
	ID3D11PixelShader* m_pshader;
	ID3D11VertexShader* m_vshader;
	ID3D11ShaderResourceView* SResViewTex[4];

	std::vector<VertexPositionNormalTextureTangent> vertexBufferData;
	
	float gameTime;

	
	void setState();
	void loadTextures();
	
};

