struct VertexOut
{
	float3 PosW		: POSITION;
	float2 SizeW	: SIZE;
	float4 Color	: COLOR;
	uint Type		: TYPE;
	uint Texture	: TEX;
	uint Rotation	: ROTATION;
};



struct GeoOut
{
	float4 PosH : SV_Position;
	float4 Color : COLOR;
	float2 Tex : TEXCOORD;
	uint Texture	: TEX;
};

#include "particle_cbuffer.hlsl"



// The draw GS just expands points into camera facing quads.
[maxvertexcount(4)]
void main(point VertexOut gin[1],
	inout TriangleStream<GeoOut> triStream)
{
	float2 gQuadTexC[4][4] =			//wszystkie obroty o wielokrotnosci 90
	{	
		{
			float2(0.0f, 0.0f),
			float2(0.0f, 1.0f),
			float2(1.0f, 0.0f),
			float2(1.0f, 1.0f),
		},
		{
			float2(1.0f, 0.0f),
			float2(0.0f, 0.0f),			
			float2(1.0f, 1.0f),
			float2(0.0f, 1.0f),
		},
		{
			float2(1.0f, 1.0f),
			float2(1.0f, 0.0f),
			float2(0.0f, 1.0f),
			float2(0.0f, 0.0f),			
		},
		{
			float2(0.0f, 1.0f),
			float2(1.0f, 1.0f),
			float2(0.0f, 0.0f),			
			float2(1.0f, 0.0f),			
		},
		
		
	};
	// do not draw emitter particles.
	if (gin[0].Type != 0)
	{
		float4 v[4];

		if (gin[0].Type < 3)
		{
			//
			// Compute world matrix so that billboard faces the camera.
			//
			float3 look = normalize(gin[0].PosW - gEyePosW.xyz);
			float3 right = normalize(cross(float3(0, 1, 0), look));
			float3 up = cross(look, right);
			//
			// Compute triangle strip vertices (quad) in world space.
			//
			float halfWidth = 0.5f*gin[0].SizeW.x;
			float halfHeight = 0.5f*gin[0].SizeW.y;
			
			v[0] = float4(gin[0].PosW + halfWidth*right - halfHeight*up, 1.0f);
			v[1] = float4(gin[0].PosW + halfWidth*right + halfHeight*up, 1.0f);
			v[2] = float4(gin[0].PosW - halfWidth*right - halfHeight*up, 1.0f);
			v[3] = float4(gin[0].PosW - halfWidth*right + halfHeight*up, 1.0f);
			
		}
		else
		{
			//plasko po wodzie
			float3 right = float3(-1, 0, 0);
			float3 up = float3(0,0,-1);
			float3 pos = gin[0].PosW;
			pos.y = -0.09f;
			float halfWidth = 0.5f*gin[0].SizeW.x;
			float halfHeight = 0.5f*gin[0].SizeW.y;
			
			v[0] = float4(pos + halfWidth*right - halfHeight*up, 1.0f);
			v[1] = float4(pos + halfWidth*right + halfHeight*up, 1.0f);
			v[2] = float4(pos - halfWidth*right - halfHeight*up, 1.0f);
			v[3] = float4(pos - halfWidth*right + halfHeight*up, 1.0f);

		}
		//
		// Transform quad vertices to world space and output
		// them as a triangle strip.
		//
		GeoOut gout;
		[unroll]
		for (int i = 0; i < 4; ++i)
		{
			gout.PosH = mul(gViewProj, v[i]);
			gout.Tex = gQuadTexC[gin[0].Rotation][i];
			gout.Color = gin[0].Color;
			gout.Texture = gin[0].Texture;
			triStream.Append(gout);
		}
	}
}
