#pragma once

#include "directxDevice.h"
#include "VertexTypes.h"
#include <WICTextureLoader.h>
#include <DDSTextureLoader.h>
#include <Mouse.h>
#include <Keyboard.h>

#include "shapes.h"
#include "light.h"
#include "timeControl.h"
#include "camera.h"
#include <vector>
#include <string>
#include "mesh.h"
#include "particleSystem.h"
#include "waterSurface.h"
#include "meshLoader.h"
#include "vertexTypes2.h"
#include "programData.h"
#include "defaultMaterial.h"
#include "terrain.h"




class control
{
public:
	control(HWND hwnd);
	~control();
	void init();
	void loop();
private:
	programData* prData;
	DirectxDevice* gDevice;
	timeControl mTimeControl;
	camera m_camera;
	
	DirectX::Keyboard::KeyboardStateTracker kTracker;
	std::vector <VertexPositionNormalTextureTangent> vertex;
	int vertexBufferSize;
	ID3D11Buffer* vertexBuffer;
	
	
	
	//ID3D11Buffer* VSConstBuffer;
	//ID3D11Buffer* PSConstBuffer;
	DirectX::SimpleMath::Matrix worldMatrix;
	//DirectX::SimpleMath::Matrix viewMatrix;
	//DirectX::SimpleMath::Matrix projMatrix;
	float animation;
	
	//VSData m_VSData;
	//PSData m_PSData;
	

	

	//stan domyslny
	ID3D11BlendState* transparentBlendState;
	ID3D11InputLayout* inLayout;
	//ID3D11PixelShader* m_pshader;
	//ID3D11VertexShader* m_vshader;
	ID3D11SamplerState* m_sampleState;

	ID3D11ShaderResourceView* SResViewTexPlazma;
	ID3D11ShaderResourceView* SResViewTexWater;
	ID3D11ShaderResourceView* SResViewParticle;



	waterSurface* water;
	defaultMaterial* defMaterial;



	particleSystem* m_particlesystem;
	/*particleSystem* m_particlesystem2;
	particleSystem* m_particlesystem3;
	particleSystem* m_particlesystem4;
	particleSystem* m_particlesystem5;*/
	
	mesh* teren;
	mesh* woda1;
	mesh* piana_decal;
	mesh* woda3;
	mesh* kula;
	mesh* woda_gorna;




	//testowanie koloru wody
	float ambient;
	float Specular;
	float Diffuse;
	float Sphardness;

	float height_mul;  //mnozniki wymirow
	float width_mul;
	float speed_mul;
	bool objectsToUpdate;

	terrain mTerrain;


	void setDefaultState();
	mesh* addMesh(std::vector <VertexPositionNormalTextureTangent> meshVertex, meshMaterial* mat);
	void createObjects();
	void createParticleSystems();
	void keyboardInput();
	void updateObjects();
};

