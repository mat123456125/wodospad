#include "directxDevice.h"

using namespace DirectX;
DirectxDevice::DirectxDevice()
{
	//Constructor
}

void DirectxDevice::Initialize(HWND hwnd, bool windowed)
{
	m_hwnd = hwnd;
	SimpleMath::Vector2 m_vec2;
	
	D3D_FEATURE_LEVEL fLevels[] =
	{
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
		D3D_FEATURE_LEVEL_9_3
	};
	UINT numFLevels = ARRAYSIZE(fLevels);

	GetClientRect(hwnd, &rectange);
	DXGI_SWAP_CHAIN_DESC swapDesc;

	ZeroMemory(&swapDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	swapDesc.BufferCount = 1;
	swapDesc.BufferDesc.Width = rectange.right;
	swapDesc.BufferDesc.Height = rectange.bottom;
	swapDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	//swapDesc.BufferDesc.RefreshRate
	swapDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapDesc.OutputWindow = hwnd;
	swapDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	swapDesc.Windowed = windowed;
	swapDesc.SampleDesc.Count = 1;
	swapDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	HRESULT result;
#ifdef _DEBUG
	result = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL,D3D11_CREATE_DEVICE_BGRA_SUPPORT|D3D11_CREATE_DEVICE_DEBUG, fLevels, numFLevels, D3D11_SDK_VERSION,
		&swapDesc, &swapChain, &dxDevice, &featureLevel, &dxContext);
#endif
#ifndef _DEBUG
	result = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, D3D11_CREATE_DEVICE_BGRA_SUPPORT , fLevels, numFLevels, D3D11_SDK_VERSION,
		&swapDesc, &swapChain, &dxDevice, &featureLevel, &dxContext);
#endif
	
	if (FAILED(result))
	{
		return;
	}

	// 2d ----
	auto options = D2D1_FACTORY_OPTIONS();
	options.debugLevel = D2D1_DEBUG_LEVEL_INFORMATION;
	result = D2D1CreateFactory(D2D1_FACTORY_TYPE_MULTI_THREADED, options, &d2dFactory);
	if (FAILED(result)) {
		return;
	}

	result = DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory), reinterpret_cast<IUnknown * *>(&wrFactory));
	if (FAILED(result)) {
		return;
	}
	//-----




	//viewport z bufora
	ID3D11Texture2D* backBufferTex = 0;
	swapChain->GetBuffer(NULL, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&backBufferTex));
	dxDevice->CreateRenderTargetView(backBufferTex, nullptr, &renderView);

	D3D11_TEXTURE2D_DESC depthBufferDesc;
	ZeroMemory(&depthBufferDesc, sizeof(D3D11_TEXTURE2D_DESC));
	depthBufferDesc.Height = rectange.bottom;
	depthBufferDesc.Width = rectange.right;
	depthBufferDesc.MipLevels = 1;
	depthBufferDesc.ArraySize = 1;
	depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthBufferDesc.SampleDesc.Count = 1;
	depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;

	dxDevice->CreateTexture2D(&depthBufferDesc, 0, &depthBuffer);
	

	//stan testu glebokosci
	D3D11_DEPTH_STENCIL_DESC depthStensilDesc;
	ZeroMemory(&depthStensilDesc, sizeof(D3D11_DEPTH_STENCIL_DESC));

	// Depth test parameters
	depthStensilDesc.DepthEnable = true;
	depthStensilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthStensilDesc.DepthFunc = D3D11_COMPARISON_LESS;

	// Stencil test parameters
	depthStensilDesc.StencilEnable = false;
	depthStensilDesc.StencilReadMask = 0;
	depthStensilDesc.StencilWriteMask = 0;

	// Stencil operations if pixel is front-facing
	depthStensilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStensilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthStensilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStensilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Stencil operations if pixel is back-facing
	depthStensilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStensilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthStensilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStensilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Create depth stencil state
	
	
	dxDevice->CreateDepthStencilState(&depthStensilDesc, &defaultDSState);


	depthStensilDesc.DepthEnable = false;
	depthStensilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;	
	dxDevice->CreateDepthStencilState(&depthStensilDesc, &disableDSState);


	depthStensilDesc.DepthEnable = true;
	depthStensilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
	dxDevice->CreateDepthStencilState(&depthStensilDesc, &readonlyDSState);


	dxContext->OMSetDepthStencilState(defaultDSState, 1);

	dxDevice->CreateDepthStencilView(depthBuffer, 0, &depthStencilView);
	

	dxContext->OMSetRenderTargets(1, &renderView,depthStencilView);
	
	//viewport
	viewPort.Width = rectange.right;
	viewPort.Height = rectange.bottom;
	viewPort.TopLeftX = 0;
	viewPort.TopLeftY = 0;
	viewPort.MinDepth = 0.0f;
	viewPort.MaxDepth = 1.0f;
	
	dxContext->RSSetViewports(1, &viewPort);

	//rasterizer

	rasterizerState.FillMode = D3D11_FILL_SOLID;
	rasterizerState.CullMode = D3D11_CULL_BACK;
	rasterizerState.FrontCounterClockwise = false;
	rasterizerState.DepthBias = 0;
	rasterizerState.DepthBiasClamp = 0;
	rasterizerState.SlopeScaledDepthBias = 0;
	rasterizerState.DepthClipEnable = true;
	rasterizerState.ScissorEnable = false;
	rasterizerState.MultisampleEnable = true;
	rasterizerState.AntialiasedLineEnable = false;

	result = dxDevice->CreateRasterizerState(&rasterizerState, &g_pRasterState); //ustawienie stanu scissor enable(nie wiem)
	if (FAILED(result)) {
		return;
	}
	dxContext->RSSetState(g_pRasterState);
	

	//2d -----
	IDXGISurface* dxgiSurf = 0;
	result = swapChain->GetBuffer(NULL, __uuidof(IDXGISurface), reinterpret_cast<void**>(&dxgiSurf));
	if (FAILED(result)) {
		return;
	}

	float dpiX;
	float dpiY;
	d2dFactory->GetDesktopDpi(&dpiX, &dpiY);

	auto d2dRTProps = D2D1::RenderTargetProperties(D2D1_RENDER_TARGET_TYPE_DEFAULT, D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED), dpiX, dpiY);

	result = d2dFactory->CreateDxgiSurfaceRenderTarget(dxgiSurf, &d2dRTProps, &d2dRenderTarget);
	if (FAILED(result)) {
		return;
	}

	//formaty i kolory textu
	result = d2dRenderTarget->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Red), &d2dSolidBrush);
	if (FAILED(result)) {
		return;
	}

	result = wrFactory->CreateTextFormat(L"Consolas", nullptr, DWRITE_FONT_WEIGHT_NORMAL, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, 14.0f, L"", &wrFormat);
	if (FAILED(result)) {
		return;
	}

	
	createSpecialRenderTargets();
}


float DirectxDevice::aspectRatio()
{
	GetClientRect(m_hwnd, &rectange);
	return (float)rectange.right / (float)rectange.bottom;
	
}

void DirectxDevice::disableDepth()
{
	dxContext->OMSetDepthStencilState(disableDSState, 1);
}

void DirectxDevice::enableDepth()
{
	dxContext->OMSetDepthStencilState(defaultDSState, 1);
}

void DirectxDevice::readonlyDepth()
{
	dxContext->OMSetDepthStencilState(readonlyDSState, 1);
}

void DirectxDevice::setSpecialRenderTarg()
{
	dxContext->OMSetRenderTargets(2, blendingRenderTarget, depthStencilView);
}

void DirectxDevice::setDefaultRenderTarg()
{
	dxContext->OMSetRenderTargets(1, &renderView, depthStencilView);
}

void DirectxDevice::present()
{
	swapChain->Present(0, 0);
}

void DirectxDevice::createSpecialRenderTargets()
{
	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));
	textureDesc.Height = rectange.bottom;
	textureDesc.Width = rectange.right;
	textureDesc.MipLevels = 1;
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_R16G16B16A16_FLOAT;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE; //uzywane takze w shaderach

	dxDevice->CreateTexture2D(&textureDesc, 0, &accumTexture);

	textureDesc.Format = DXGI_FORMAT_R16_FLOAT;
	dxDevice->CreateTexture2D(&textureDesc, 0, &revealageTexture);

	dxDevice->CreateRenderTargetView(accumTexture, nullptr, &blendingRenderTarget[0]);
	dxDevice->CreateRenderTargetView(revealageTexture, nullptr, &blendingRenderTarget[1]);
	HRESULT res = dxDevice->CreateShaderResourceView(accumTexture, nullptr, &blendingShaderResource[0]);
	dxDevice->CreateShaderResourceView(revealageTexture, nullptr, &blendingShaderResource[1]);
}

DirectxDevice::~DirectxDevice()
{
	if (dxContext)dxContext->ClearState();
	renderView->Release();
	swapChain->Release();
	dxContext->Release();
	dxDevice->Release();
	d2dFactory->Release();
	d2dSolidBrush->Release();
	d2dRenderTarget->Release();
}