#include "terrain.h"

#define TEX_SCALE 1.0f
#define COLL_BUFF 0.2f  //zapas wysokosci plaszczyzny kolizji

using namespace DirectX;
using namespace SimpleMath;
using namespace std;



terrain::terrain()
{
	upper_length = 8.5f;
	lower_length = 8.5f;
	width = 22.5f;
	height = 1.6f;
	cliff_pos = 0;

	lower_ch.width_up = 1.8;
	lower_ch.width_down = 1.4;
	lower_ch.depth = 0.2;
	upper_ch.width_up = 1.0;
	upper_ch.width_down = 0.8;
	upper_ch.depth = 0.2;
}

std::vector<VertexPositionNormalTextureTangent> terrain::generateVertex()
{
	vector<VertexPositionNormalTextureTangent> ret_vertex;
	vector<VertexPositionNormalTextureTangent> temp;

	temp = generateChannelVertex(true);
	ret_vertex.insert(ret_vertex.end(), temp.begin(), temp.end());
	temp = generateChannelVertex(false);
	ret_vertex.insert(ret_vertex.end(), temp.begin(), temp.end());
	temp = generateHorizontalVertex();
	ret_vertex.insert(ret_vertex.end(), temp.begin(), temp.end());
	temp = generateVerticalVertex();
	ret_vertex.insert(ret_vertex.end(), temp.begin(), temp.end());




	return ret_vertex;
}

void terrain::moveCliff(float deltha)
{
	cliff_pos = cliff_pos + deltha;
}

void terrain::changeWidth(float deltha)
{
	lower_ch.width_up += deltha;
	lower_ch.width_down += deltha;
	upper_ch.width_up += deltha;
	upper_ch.width_down += deltha;
}

void terrain::changeHeight(float deltha)
{
	height = height + deltha;
}

DirectX::SimpleMath::Plane terrain::getCollisionPlane()
{
	Vector3 a = Vector3(0, -lower_ch.depth + COLL_BUFF, 0);
	Vector3 b = Vector3(-upper_ch.width_down, height -upper_ch.depth + COLL_BUFF, cliff_pos);
	Vector3 c = Vector3(upper_ch.width_down, height -upper_ch.depth + COLL_BUFF, cliff_pos);

	return Plane(a, c, b);
}

std::vector<VertexPositionNormalTextureTangent> terrain::generateChannelVertex(bool upDown)
{
	vector<VertexPositionNormalTextureTangent> ret_vertex;
	vector<VertexPositionNormalTextureTangent> temp;

	
	float start_y = 0;
	float start_z = lower_length;
	float start_x = -lower_ch.width_up / 2.0;
	float wall_x_diff = (lower_ch.width_up - lower_ch.width_down) / 2.0;
	float temp_length = -lower_length;
	float temp_depth = lower_ch.depth;
	float wall_bottom_width = wall_x_diff + lower_ch.width_down;
	float temp_up_width = lower_ch.width_up;

	if (upDown)
	{
		start_y = height;
		start_z = cliff_pos;
		start_x = -upper_ch.width_up / 2.0;
		wall_x_diff = (upper_ch.width_up - upper_ch.width_down) / 2.0;
		temp_length = -upper_length;
		temp_depth = upper_ch.depth;
		wall_bottom_width = wall_x_diff + upper_ch.width_down;
		temp_up_width = upper_ch.width_up;		

	}
	

	temp = generateQuad(Vector3(start_x, start_y, start_z), Vector3(start_x, start_y, start_z + temp_length),
		Vector3(start_x + wall_x_diff, start_y - temp_depth, start_z), Vector3(start_x + wall_x_diff, start_y - temp_depth, start_z + temp_length));
	ret_vertex.insert(ret_vertex.end(), temp.begin(), temp.end());

	temp = generateQuad(Vector3(start_x + wall_x_diff, start_y - temp_depth, start_z), Vector3(start_x + wall_x_diff, start_y - temp_depth, start_z + temp_length),
		Vector3(start_x + wall_bottom_width, start_y - temp_depth, start_z), Vector3(start_x + wall_bottom_width, start_y - temp_depth, start_z + temp_length));
	ret_vertex.insert(ret_vertex.end(), temp.begin(), temp.end());

	temp = generateQuad(Vector3(start_x + wall_bottom_width, start_y - temp_depth, start_z), Vector3(start_x + wall_bottom_width, start_y - temp_depth, start_z + temp_length),
		Vector3(start_x + temp_up_width, start_y , start_z), Vector3(start_x + temp_up_width, start_y, start_z + temp_length));
	ret_vertex.insert(ret_vertex.end(), temp.begin(), temp.end());



	
	return ret_vertex;
}

std::vector<VertexPositionNormalTextureTangent> terrain::generateHorizontalVertex()
{
	vector<VertexPositionNormalTextureTangent> ret_vertex;
	vector<VertexPositionNormalTextureTangent> temp;

	float start_x = -width / 2.0;
	float channel_start = -upper_ch.width_up / 2.0;

	temp = generateQuad(Vector3(start_x, height, cliff_pos), Vector3(start_x, height, cliff_pos - upper_length),
		Vector3(channel_start, height, cliff_pos), Vector3(channel_start, height, cliff_pos - upper_length));
	ret_vertex.insert(ret_vertex.end(), temp.begin(), temp.end());

	temp = generateQuad(Vector3(-channel_start, height, cliff_pos), Vector3(-channel_start, height, cliff_pos - upper_length),
		Vector3(-start_x, height, cliff_pos), Vector3(-start_x, height, cliff_pos - upper_length));
	ret_vertex.insert(ret_vertex.end(), temp.begin(), temp.end());

	
	channel_start = -lower_ch.width_up / 2.0;

	temp = generateQuad(Vector3(start_x, 0, lower_length), Vector3(start_x, 0, 0),
		Vector3(channel_start, 0, lower_length), Vector3(channel_start, 0, 0));
	ret_vertex.insert(ret_vertex.end(), temp.begin(), temp.end());

	temp = generateQuad(Vector3(-channel_start, 0, lower_length), Vector3(-channel_start, 0, 0),
		Vector3(-start_x, 0, lower_length), Vector3(-start_x, 0, 0));
	ret_vertex.insert(ret_vertex.end(), temp.begin(), temp.end());




	return ret_vertex;
}

std::vector<VertexPositionNormalTextureTangent> terrain::generateVerticalVertex()
{
	vector<VertexPositionNormalTextureTangent> ret_vertex;
	vector<VertexPositionNormalTextureTangent> temp;

	float start_x = -width / 2.0;
	float channel_up_start = -upper_ch.width_up / 2.0;
	float channel_down_start = -lower_ch.width_up / 2.0;
	float up_bottom_x = -upper_ch.width_down / 2.0;
	float down_bottom_x = -lower_ch.width_down / 2.0;

	temp = generateQuad(Vector3(start_x, height, cliff_pos), Vector3(channel_up_start, height, cliff_pos ),
		Vector3(start_x, 0, 0), Vector3(channel_down_start, 0, 0));
	ret_vertex.insert(ret_vertex.end(), temp.begin(), temp.end());

	temp = generateQuad(Vector3(-channel_up_start, height, cliff_pos), Vector3(-start_x, height, cliff_pos),
		Vector3(-channel_down_start, 0, 0), Vector3(-start_x, 0, 0));
	ret_vertex.insert(ret_vertex.end(), temp.begin(), temp.end());


	temp = generateQuad(Vector3(channel_up_start, height, cliff_pos), Vector3(up_bottom_x, height - upper_ch.depth, cliff_pos),
		Vector3(channel_down_start, 0, 0), Vector3(down_bottom_x, - lower_ch.depth, 0));
	ret_vertex.insert(ret_vertex.end(), temp.begin(), temp.end());

	temp = generateQuad(Vector3(-up_bottom_x, height - upper_ch.depth, cliff_pos), Vector3(-channel_up_start, height, cliff_pos),
		Vector3(-down_bottom_x, -lower_ch.depth, 0), Vector3(-channel_down_start, 0, 0));
	ret_vertex.insert(ret_vertex.end(), temp.begin(), temp.end());

	temp = generateQuad(Vector3(up_bottom_x, height - upper_ch.depth, cliff_pos), Vector3(-up_bottom_x, height - upper_ch.depth, cliff_pos),
		Vector3(down_bottom_x, -lower_ch.depth, 0), Vector3(-down_bottom_x, -lower_ch.depth, 0));
	ret_vertex.insert(ret_vertex.end(), temp.begin(), temp.end());


	

	return ret_vertex;
}

std::vector<VertexPositionNormalTextureTangent> terrain::generateQuad(Vector3 ul, Vector3 ur, Vector3 ll, Vector3 lr)
{
	vector<VertexPositionNormalTextureTangent> ret_vertex;
	VertexPositionNormalTextureTangent temp;
	

	temp.normal = setNormal(ul, ur, ll);

	temp.position = ul;
	temp.textureCoordinate = Vector2(0, 0);
	ret_vertex.push_back(temp);
	temp.position = ur;
	temp.textureCoordinate = Vector2((ur - ul).Length()*TEX_SCALE, 0);
	ret_vertex.push_back(temp);
	temp.position = ll;
	temp.textureCoordinate = Vector2(0,(ll - ul).Length()*TEX_SCALE);
	ret_vertex.push_back(temp);

	temp.normal = setNormal(ll, ur, lr);

	ret_vertex.push_back(temp);

	temp.position = ur;
	temp.textureCoordinate = Vector2((lr - ll).Length()*TEX_SCALE, 0);
	ret_vertex.push_back(temp);

	temp.position = lr;
	temp.textureCoordinate = Vector2((lr - ll).Length()*TEX_SCALE, (ll - ul).Length()*TEX_SCALE);
	ret_vertex.push_back(temp);
	


	return ret_vertex;
}

Vector3 terrain::setNormal(Vector3 v1, Vector3 v2, Vector3 v3)
{
	Vector3 temp1, temp2; //do obliczania wektora normalnego
	temp1 = v2 - v1;
	temp2 = v3 - v1;
	temp1 = temp2.Cross(temp1);
	temp1.Normalize();
	return temp1;
}


