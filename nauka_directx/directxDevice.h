#ifndef DIRECTXDEVICE_H
#define DIRECTXDEVICE_H

#include <DirectXColors.h>
#include <d3d11.h>
#include <d2d1.h>
#include <SimpleMath.h>
#include <dxgi.h>
#include <dwrite.h>


#include <string.h>






class DirectxDevice
{
public:
	DirectxDevice();
	~DirectxDevice();

	void Initialize(HWND hwnd, bool windowed);

	float aspectRatio();
	void disableDepth();
	void enableDepth();
	void readonlyDepth();
	void setSpecialRenderTarg();
	void setDefaultRenderTarg();
	void present();

	HWND m_hwnd;
	ID3D11Device* dxDevice;
	ID3D11DeviceContext* dxContext;
	IDXGISwapChain* swapChain;
	ID3D11RenderTargetView* renderView; //3d
	RECT rectange; //rozmiar wnetrza okna

	ID2D1Factory* d2dFactory;
	IDWriteFactory* wrFactory;
	ID2D1RenderTarget*  d2dRenderTarget;  //text w 2d
	ID2D1SolidColorBrush*  d2dSolidBrush;
	IDWriteTextLayout*  wrLayout;
	IDWriteTextFormat* wrFormat;


	D3D_DRIVER_TYPE driverType;
	D3D_FEATURE_LEVEL featureLevel;
	D3D11_VIEWPORT viewPort;
	ID3D11RasterizerState* g_pRasterState;
	D3D11_RASTERIZER_DESC rasterizerState;
	ID3D11DepthStencilState * defaultDSState;
	ID3D11DepthStencilState * disableDSState;
	ID3D11DepthStencilState * readonlyDSState;
	ID3D11Texture2D* depthBuffer;
	ID3D11DepthStencilView* depthStencilView;

	//particles blending 

	ID3D11Texture2D* accumTexture;
	ID3D11Texture2D* revealageTexture;
	ID3D11RenderTargetView* blendingRenderTarget[2];
	ID3D11ShaderResourceView* blendingShaderResource[2];
	
	

private:
	void createSpecialRenderTargets();


};

#endif /* DIRECTXDEVICE_H */