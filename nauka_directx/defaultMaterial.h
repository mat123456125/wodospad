#pragma once

#include "meshMaterial.h"
#include "programData.h"
#include "directxDevice.h"

class defaultMaterial :public meshMaterial
{
public:
	defaultMaterial(programData* pd);
	~defaultMaterial();
	virtual void set();
private:
	programData* prData;
	DirectxDevice* gDevice;
	ID3D11PixelShader* m_pshader;
	ID3D11VertexShader* m_vshader;
};

