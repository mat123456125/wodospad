#pragma once
#include <assimp\Importer.hpp>
#include <assimp\scene.h>
#include <assimp\postprocess.h>
#include <string>
#include <vector>
#include <VertexTypes.h>
#include <SimpleMath.h>
#include "vertexTypes2.h"

class meshLoader
{
public:
	static std::vector<DirectX::VertexPositionNormalTexture> loadVPNT(std::string file);
	static std::vector<VertexPositionNormalTextureTangent> loadVPNTT(std::string file);
private:
	static const aiScene* loadScene(std::string file);
	

	
};

