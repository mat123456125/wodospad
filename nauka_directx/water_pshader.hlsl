#include "light.hlsl"

Texture2D gDiffuseTex		: register(t0);
Texture2D gDiffuseTex2		: register(t1);
Texture2D gNormalMap		: register(t2);
Texture2D gNormalMap2		: register(t3);
SamplerState samAnisotropic;

cbuffer cbframe : register(b0)
{
	directionalLight gDirLight;
	pointLight gPointLight;
	spotLight gSpotLight;
	float3 gEyePosW;
	float pad;
};
cbuffer cbobject
{
	material gMaterial;
	float gGameTime;
	
};

struct VertexOut
{
	float4 PosH : SV_POSITION;
	float3 PosW : POSITION;
	float3 NormalW : NORMAL;
	float3 TangentW : TANGENT;
	float2 Tex : TEXCOORD;
	float3 Foam : FOAM;
};

float4 PS(VertexOut pin) : SV_Target
{
	float2 texCoord1 = float2(0.0f, -0.04f)*gGameTime;
	float2 texCoord2 = float2(0.0f, -0.12f)*gGameTime;
	texCoord1 = texCoord2+ pin.Tex;
	//pin.Tex.
	texCoord2 = texCoord2 + pin.Tex*0.6f;
	float4 texColor = gDiffuseTex.Sample(samAnisotropic, texCoord1);
	float3 normalMapSample2 = gDiffuseTex2.Sample(samAnisotropic, texCoord2*0.5f).rgb;
	//texColor = lerp(texColor, float4(0.88, 0.88, 0.92, 1),pin.Foam.x);
	// Interpolating normal can unnormalize it, so normalize it.
	pin.NormalW = normalize(pin.NormalW);
	float3 toEyeW = normalize(gEyePosW - pin.PosW);




	// Normal mapping
	//
	float2 normalCoord1 = float2(0.008f, -0.005f)*gGameTime;	
	float2 normalCoord2 = float2(-0.008f, -0.04f)*gGameTime;
	normalCoord1 = normalCoord1 + pin.Tex;
	normalCoord2 = normalCoord2 + pin.Tex;

	float3 normalMapSample = gNormalMap.Sample(samAnisotropic, normalCoord1).rgb +3*gNormalMap2.Sample(samAnisotropic, normalCoord2).rgb;
	//normalMapSample = normalMapSample/4;
	normalMapSample = normalize(normalMapSample); //chyba teraz dobrze
	float3 bumpedNormalW = NormalSampleToWorldSpace(
		normalMapSample, pin.NormalW, pin.TangentW);
	float3 bumpedNormalW2 = NormalSampleToWorldSpace(
		normalMapSample2, pin.NormalW, pin.TangentW);
	//bumpedNormalW2 = pin.NormalW;

	// Start with a sum of zero.
	float4 ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
	float4 diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
	float4 spec = float4(0.0f, 0.0f, 0.0f, 0.0f);

	// Sum the light contribution from each light source.
	float4 A, D, S;
	ComputeDirectionalLight(gMaterial, gDirLight, bumpedNormalW, toEyeW, A, D, S);
	ambient += A;
	diffuse += D;
	spec += S;

	ComputePointLight(gMaterial, gPointLight,	pin.PosW, bumpedNormalW, toEyeW, A, D, S);
	ambient += A;
	diffuse += D;
	spec += S;

	/*ComputeSpotLight(gMaterial, gSpotLight, pin.PosW, bumpedNormalW, toEyeW, A, D, S);
	ambient += A;
	diffuse += D;
	spec += S;*/

	/*float4 litColor = texColor*(ambient*(1 + 4 * pin.Foam.x) + diffuse*(1 - 0.7*pin.Foam.x)) + spec*(1 - pin.Foam.x);*/
	//texColor = lerp(texColor, texColor2, pin.Foam.x);
	float4 litColor = texColor*(ambient + diffuse) + spec;

	//swiatlo dla spadku
	ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
	diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
	spec = float4(0.0f, 0.0f, 0.0f, 0.0f);

	ComputeDirectionalLight(gMaterial, gDirLight, bumpedNormalW2, toEyeW, A, D, S);
	ambient += A;
	diffuse += D;
	spec += S;

	ComputePointLight(gMaterial, gPointLight, pin.PosW, bumpedNormalW2, toEyeW, A, D, S);
	ambient += A;
	diffuse += D;
	spec += S;

	float4 litColor2 = texColor*(ambient + diffuse) + spec;
	
	//litColor = lerp(litColor, float4(0.88, 0.88, 0.92, 1), pin.Foam.x);
	litColor = lerp(litColor, litColor2, pin.Foam.x);
	litColor.a = gMaterial.Diffuse.a;
	return litColor;
}