#include "particle_cbuffer.hlsl"

struct Vertex
{
	float3 PosW			: POSITION;
	float3 VelW			: VELOCITY;
	float2 SizeW		: SIZE;
	float Age			: AGE;
	uint Type			: TYPE;
	uint Texture		: TEX; // indeks w tablicy tekstur
	uint Rotation		: ROTATION;  //obroty o wielokrotnosc 90 stopni
};

Vertex main(Vertex vin)
{
	Vertex vout;
	if (vin.Type != 0)
	{
		float t = gTimeStep;
		
		float3 Pos = 0.5f*t*t*gAccel + t*vin.VelW +vin.PosW;
		vout.VelW =  t*gAccel + vin.VelW;
		vout.SizeW = vin.SizeW +0.2f*t;
		vout.Age = vin.Age;
		vout.Type = vin.Type;
		vout.Texture = vin.Texture;
		vout.Rotation = vin.Rotation;
		//odbicie

		float dist = dot(collPlane, float4(Pos, 1.0f)); //odleglosc punktu od plaszczyzny odbicia
		if (dist < 0)
		{
			vout.VelW = vout.VelW - 2.0f * collPlane.xyz *dot(collPlane.xyz, vout.VelW);
			Pos = Pos - dist * collPlane.xyz;
		}

		vout.PosW = Pos;
	}
	else
		vout = vin;
	return vout;
}