#pragma once
#include <SimpleMath.h>
#include "directxDevice.h"
#include <Mouse.h>
#include <Keyboard.h>

class camera
{
public:
	camera();
	~camera();
	DirectX::SimpleMath::Matrix computeCamera();

	DirectX::SimpleMath::Vector3 camPos;
	float angleX, angleY,zoom;

	DirectX::Mouse::ButtonStateTracker tracker;

};

