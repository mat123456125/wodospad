#pragma once

#include "directxDevice.h"
#include "light.h"
#include <Mouse.h>
#include <Keyboard.h>

struct PSDataFrame
{
	directionalLight gDirLight;
	pointLight gPointLight;
	spotLight gSpotLight;
	DirectX::SimpleMath::Vector3 gEyePosW;
	float pad;
};

struct PSDataObject
{
	objectLighting gLighting;
	float gameTime;
	DirectX::SimpleMath::Vector3 pad;
};

struct VSData
{
	DirectX::SimpleMath::Matrix finalMatrix;
	DirectX::SimpleMath::Matrix worldMatrix;
	DirectX::SimpleMath::Matrix worldInvTransMatrix;
};

struct programData
{
	DirectxDevice* gDevice;

	DirectX::Mouse* m_mouse;
	DirectX::Keyboard* m_keyboard;

	VSData m_VSData;
	PSDataFrame m_PSDataFrame;
	PSDataObject m_PSDataObject;

	ID3D11Buffer* VSConstBuffer;
	ID3D11Buffer* PSFrameConstBuffer;
	ID3D11Buffer* PSObjectConstBuffer;

	DirectX::SimpleMath::Matrix viewMatrix;
	DirectX::SimpleMath::Matrix projMatrix;
	DirectX::SimpleMath::Matrix viewProjMatrix;
	
	DirectX::SimpleMath::Plane collisionPlane;

	double deltaTime;
};