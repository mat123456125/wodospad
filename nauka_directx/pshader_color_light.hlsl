#include "light.hlsl"

cbuffer cbdata : register(b0)
{
	material gMaterial;
	directionalLight gDirLight;
	pointLight gPointLight;
	spotLight gSpotLight;
	float3 gEyePosW;
	float pad;
};

struct VertexOut
{
	float4 PosH : SV_POSITION;
	float3 PosW : POSITION;
	float3 Normal : NORMAL;
	float4 Color : COLOR;
};

float4 PS(VertexOut pin) : SV_Target
{


	// Interpolating normal can unnormalize it, so normalize it.
	pin.Normal = normalize(pin.Normal);
float3 toEyeW = normalize(gEyePosW - pin.PosW);

// Start with a sum of zero.
float4 ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
float4 diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
float4 spec = float4(0.0f, 0.0f, 0.0f, 0.0f);

// Sum the light contribution from each light source.
float4 A, D, S;
ComputeDirectionalLight(gMaterial, gDirLight,	pin.Normal, toEyeW, A, D, S);
ambient += A;
diffuse += D;
spec += S;

ComputePointLight(gMaterial, gPointLight,	pin.PosW, pin.Normal, toEyeW, A, D, S);
ambient += A;
diffuse += D;
spec += S;

ComputeSpotLight(gMaterial, gSpotLight, pin.PosW, pin.Normal, toEyeW, A, D, S);
ambient += A;
diffuse += D;
spec += S;

float4 litColor = pin.Color * (ambient + diffuse) + spec;

litColor.a = gMaterial.Diffuse.a;
return litColor;
}