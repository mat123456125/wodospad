#pragma once
#include "VertexTypes.h"
#include "vertexTypes2.h"
#include "SimpleMath.h"
#include <vector>
#include <math.h>




class plain
{
private:
public:
	plain(DirectX::SimpleMath::Vector2 size, int steps_x, int steps_z);
	plain(DirectX::SimpleMath::Vector2 size, int steps_x, int steps_z,int steps_y); // zagiecie na gorze wodospadu
	void wave();
	std::vector <DirectX::SimpleMath::Vector3> points;
	std::vector <DirectX::SimpleMath::Vector2> texCoord;
	std::vector <DirectX::SimpleMath::Vector3> foam;
	int m_stepsX;
	int m_stepsZ;
};

class shapes
{
public:
	static std::vector <DirectX::VertexPositionColor> sphereVPC(int steps, DirectX::SimpleMath::Vector4 color);
	static std::vector <DirectX::VertexPositionNormalColor> sphereVPNC(int steps, DirectX::SimpleMath::Vector4 color);
	static std::vector <DirectX::VertexPositionNormalTexture> sphereVPNT(int steps);
	static std::vector <DirectX::VertexPositionNormalColor> rectangleVPNC(DirectX::SimpleMath::Vector2 size, DirectX::SimpleMath::Vector4 color);
	static std::vector <DirectX::VertexPositionNormalTexture> rectangleVPNT(DirectX::SimpleMath::Vector2 size);
	static std::vector <VertexPositionNormalTextureTangent> rectangleVPNTT(DirectX::SimpleMath::Vector2 size);
	static std::vector <DirectX::VertexPositionNormalTexture> plainVPNT(plain pl);
	static std::vector <VertexPositionNormalTextureTangent> plainVPNTT(plain pl);
	static std::vector <VertexPositionNormalTextureTangent> upperWaterModel(float height, float length, float width, float depth, float speed, DirectX::SimpleMath::Plane collPlane); //wysokosc od dolu do powieszchni wody
private:
	class triangle
	{
	public:
		triangle();
		triangle(DirectX::SimpleMath::Vector3 a, DirectX::SimpleMath::Vector3 b, DirectX::SimpleMath::Vector3 c);
		DirectX::SimpleMath::Vector3 vertex[3];
		DirectX::SimpleMath::Vector3 triNormal; //wektor normalny dla calego trojkata
		DirectX::SimpleMath::Vector3 normal[3];
		DirectX::SimpleMath::Vector3 triTangent; //wektor styczny dla calego trojkata
		DirectX::SimpleMath::Vector3 tangent[3];
		DirectX::SimpleMath::Vector3 foam[3];
		DirectX::SimpleMath::Vector2 tex[3];
		void setNormal();
		void setTangent();
	};
	struct point
	{
		DirectX::SimpleMath::Vector3 pos;
		DirectX::SimpleMath::Vector2 texCoord;
		DirectX::SimpleMath::Vector3 foam;
	};
	struct quad
	{
		triangle upperLeft;
		triangle lowerRight;
	};
	static std::vector<triangle> basicSphere();
	static std::vector<triangle> refine(std::vector<triangle> triangles);
	static DirectX::SimpleMath::Vector3 getMiddle(DirectX::SimpleMath::Vector3 a, DirectX::SimpleMath::Vector3 b);
	static std::vector<triangle> plainTriangles(plain pl);

	static std::vector<std::vector <point>> waterPlainPoints(float length, float width);
	static std::vector<std::vector <point>> waterDropPoints(float height, float width, float speed, DirectX::SimpleMath::Plane collPlane);
	static std::vector<std::vector <point>> waterSidesPoints(float height, float depth, float speed, DirectX::SimpleMath::Plane collPlane, std::vector<std::vector <point>>* dropPoints);
	static std::vector<std::vector <shapes::quad>> waterPlainTriangles(std::vector<std::vector <point>> points);
	static std::vector<std::vector <triangle>> waterSidesTriangles(std::vector<std::vector <point>> dropPoints, std::vector<std::vector <point>> sidePoints);
	static void smoothWaterPlain(std::vector<std::vector<quad>> & plain, std::vector<std::vector<quad>> & drop);
	static void smoothWaterDropSides(std::vector<std::vector<quad>> & drop, std::vector<std::vector <triangle>> & sides, std::vector<std::vector<quad>> & plain);
	static void quadsToVertex(std::vector<std::vector<quad>> input, std::vector <VertexPositionNormalTextureTangent> & output);
	static void trianglesToVertex(std::vector<std::vector<triangle>> input, std::vector <VertexPositionNormalTextureTangent> & output);
};

