#pragma once
#include "vertexTypes2.h"
#include "SimpleMath.h"
#include <vector>


class terrain
{
private:
	class channel
	{
	public:
		float width_up;
		float width_down;
		float depth;
	};
public:
	terrain();
	
	channel upper_ch;
	channel lower_ch;
	float upper_length;
	float lower_length;
	float width;
	float height;
	float cliff_pos; // przesuniecie g�ry wzgledem do�u w osi z

	std::vector <VertexPositionNormalTextureTangent> generateVertex();
	void moveCliff(float deltha);
	void changeWidth(float deltha);
	void changeHeight(float deltha);
	DirectX::SimpleMath::Plane getCollisionPlane();
private:
	std::vector <VertexPositionNormalTextureTangent> generateChannelVertex(bool upDown); // true is up
	std::vector <VertexPositionNormalTextureTangent> generateHorizontalVertex();
	std::vector <VertexPositionNormalTextureTangent> generateVerticalVertex();
	std::vector <VertexPositionNormalTextureTangent> generateQuad(DirectX::SimpleMath::Vector3 ul, DirectX::SimpleMath::Vector3 ur,
		DirectX::SimpleMath::Vector3 ll, DirectX::SimpleMath::Vector3 lr );
	DirectX::SimpleMath::Vector3 setNormal(DirectX::SimpleMath::Vector3 v1, DirectX::SimpleMath::Vector3 v2,
		DirectX::SimpleMath::Vector3 v3);

};

