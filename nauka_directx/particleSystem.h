#pragma once

#include "directxDevice.h"
#include "randomHelper.h"
#include "programData.h"



struct Particle
{
	DirectX::SimpleMath::Vector3 Pos;
	DirectX::SimpleMath::Vector3 Vel;
	DirectX::SimpleMath::Vector2 Size;
	float Age;
	unsigned int Type;
	unsigned int Texture;
	unsigned int Rotation;
};

struct constBufferData
{
	DirectX::SimpleMath::Vector3 gEyePosW;
	float pad;
	DirectX::SimpleMath::Vector3 gInitialVel;
	float pad1;
	DirectX::SimpleMath::Vector3 gAccel;
	float pad2;
	float gGameTime;
	float gTimeStep;
	DirectX::SimpleMath::Vector2 pad3;
	DirectX::SimpleMath::Matrix gViewProj;
	DirectX::SimpleMath::Plane collPlane;
};

class particleSystem
{
public:
	particleSystem(programData* pr, UINT max_vert, std::vector<DirectX::SimpleMath::Vector3>& emitPos);
	~particleSystem();

	void Reset();
	void Update(std::vector<DirectX::SimpleMath::Vector3>& emitPos); 
	void Draw();

	
	ID3D11ShaderResourceView* mTexArray;
	DirectX::SimpleMath::Vector3 mInitialVel;
	DirectX::SimpleMath::Vector3 mAccel;

private:
	void CreateBState();
	void CreateVertexBuffers(std::vector<DirectX::SimpleMath::Vector3>& emitPos);
	void UpdateVertexBuffers(std::vector<DirectX::SimpleMath::Vector3>& emitPos);
	void CreateShaders();
	void CreateInputLayout();
	ID3D11ShaderResourceView* createRandomTexture();
	void setState();
	void setStateSO();
	void setStateFinal();


	DirectxDevice* gDevice;
	programData* prData;
	UINT max_vertices;
	UINT num_emiters;

	bool mFirstRun;
	float mGameTime;
	float mAge;
	
	
	constBufferData* m_constBufferData;
	

	ID3D11BlendState* particleBlendState;
	ID3D11BlendState* normalBlendState;
	ID3D11InputLayout* inLayout;
	ID3D11InputLayout* inLayout2;
	ID3D11SamplerState* samplerState;

	ID3D11Buffer* screenRectVB;
	ID3D11Buffer* mInitVB;
	ID3D11Buffer* vertexBuffer1;
	ID3D11Buffer* vertexBuffer2;
	ID3D11Buffer* constBuffer;

	
	ID3D11ShaderResourceView* mRandomTex;

	ID3D11PixelShader* m_pshader;
	ID3D11PixelShader* m_pshaderF;
	ID3D11VertexShader* m_vshader;
	ID3D11VertexShader* m_vshaderSO;
	ID3D11VertexShader* m_vshaderF;
	ID3D11GeometryShader* m_gshader;
	ID3D11GeometryShader* m_gshaderSO;

};

