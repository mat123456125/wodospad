Texture2D accumTex;
Texture2D revealageTex;
SamplerState samLinear;

struct VertexOut
{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD;
};


float4 PS(VertexOut pin) : SV_Target
{
	float4 accum = accumTex.Sample(samLinear, pin.Tex);
	float reveal = revealageTex.Sample(samLinear, pin.Tex).r;
	return float4(accum.rgb / max(accum.a, 1e-5), reveal);
	//return accum;
}