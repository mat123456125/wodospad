cbuffer cbdata : register(b0)
{
	float4x4 gWorldViewProj;
	float4x4 gWorld;
	float4x4 gWorldInvTrans;
};

struct VertexIn
{
	float3 Pos : SV_POSITION;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD;
	float3 TangentL : TANGENT;
	float3 Foam : FOAM;
};

struct VertexOut
{
	float4 PosH : SV_POSITION;
	float3 PosW : POSITION;
	float3 Normal : NORMAL;	
	float2 Tex : TEXCOORD;
};

VertexOut VS(VertexIn vin)
{
	VertexOut vout;
	vout.PosW = mul(gWorld,float4(vin.Pos, 1.0f) ).xyz;
	vout.Normal = mul((float3x3)gWorldInvTrans, vin.Normal);
	vout.PosH = mul(gWorldViewProj  ,float4(vin.Pos, 1.0f));
	
	vout.Tex = vin.Tex;
	return vout;
}