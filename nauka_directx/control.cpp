#include "control.h"
#include "vshader.h"
#include "pshader.h"


using namespace DirectX;
using namespace SimpleMath;
using namespace std;

control::control(HWND hwnd)
{
	gDevice = new DirectxDevice();
	gDevice->Initialize(hwnd, true);
	prData = new programData();
	prData->gDevice = gDevice;
	prData->m_mouse = new Mouse();
	prData->m_mouse->SetWindow(hwnd);
	prData->m_keyboard = new Keyboard();
	
}


control::~control()
{
	delete gDevice;
}

void control::init()
{
	//stare
	/*ambient = 0.2f;
	Specular = 0.6f;
	Diffuse = 0.4f ;
	Sphardness = 5.6f;*/
	//temp
	ambient = 0.6f;
	Specular = 0.6f;
	Diffuse = 0.2f;
	Sphardness = 5.6f;

	height_mul = 1.0f; 
	width_mul = 1.0f;
	speed_mul = 1.0f;
	objectsToUpdate =false;

	

	//

	HRESULT result;
	result = CreateWICTextureFromFile(gDevice->dxDevice, L"tex3.jpg", nullptr, &SResViewTexPlazma);
	result = CreateWICTextureFromFile(gDevice->dxDevice, L"water_tex.jpg", nullptr, &SResViewTexWater);
	result = CreateWICTextureFromFile(gDevice->dxDevice, L"water_particle.png", nullptr, &SResViewParticle);
	
	
	createParticleSystems();

	water = new waterSurface(prData);
	defMaterial = new defaultMaterial(prData);
	
	D3D11_SAMPLER_DESC samplerDesc;
	ZeroMemory(&samplerDesc, sizeof(D3D11_SAMPLER_DESC));
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	//samplerDesc.Filter = D3D11_FILTER_ANISOTROPIC;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MaxAnisotropy = 4;
	
	result = gDevice->dxDevice->CreateSamplerState(&samplerDesc, &m_sampleState);

	/*ZeroMemory(&samplerDesc, sizeof(D3D11_SAMPLER_DESC));
	samplerDesc.Filter = D3D11_FILTER_;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MaxAnisotropy = 4;

	result = gDevice->dxDevice->CreateSamplerState(&samplerDesc, &m_sampleState2);*/

	
	
	
	
	D3D11_BUFFER_DESC buffDesc;


	D3D11_SUBRESOURCE_DATA verData;
	

	HRESULT res;
	
	//shadery
	
	//res = gDevice->dxDevice->CreateVertexShader(vshader, sizeof(vshader), nullptr, &m_vshader);
	
	//res = gDevice->dxDevice->CreatePixelShader(pshader, sizeof(pshader), nullptr, &m_pshader);
	D3D11_INPUT_ELEMENT_DESC inputElements[5];
	inputElements[0] = { "SV_Position", 0, DXGI_FORMAT_R32G32B32_FLOAT,		0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 };
	inputElements[1] = { "NORMAL",      0, DXGI_FORMAT_R32G32B32_FLOAT,		0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 };
	inputElements[2] = { "TEXCOORD",    0, DXGI_FORMAT_R32G32_FLOAT,		0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 };
	inputElements[3] = { "TANGENT",		0, DXGI_FORMAT_R32G32B32_FLOAT,		0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 };
	inputElements[4] = { "FOAM",		0, DXGI_FORMAT_R32G32B32_FLOAT,		0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 };
	
	
	res = gDevice->dxDevice->CreateInputLayout(inputElements, 5, vshader, sizeof(vshader), &inLayout);
	
	

	//cbuffer dla vertex shadera
	buffDesc.ByteWidth = sizeof(VSData);
	buffDesc.Usage = D3D11_USAGE_DEFAULT;
	buffDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	buffDesc.MiscFlags = 0;
	buffDesc.StructureByteStride = 0;
	
	verData.pSysMem = &(prData->m_VSData);

	res = gDevice->dxDevice->CreateBuffer(&buffDesc, &verData, &prData->VSConstBuffer);
	
	
	
	//cbuffer dla pixel shadera
	buffDesc.ByteWidth = sizeof(PSDataFrame);
	buffDesc.Usage = D3D11_USAGE_DEFAULT;
	buffDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	buffDesc.MiscFlags = 0;
	buffDesc.StructureByteStride = 0;

	//light::prepare(&m_PSData);
	verData.pSysMem = &prData->m_PSDataFrame;

	res = gDevice->dxDevice->CreateBuffer(&buffDesc, &verData, &prData->PSFrameConstBuffer);

	buffDesc.ByteWidth = sizeof(PSDataObject);
	buffDesc.Usage = D3D11_USAGE_DEFAULT;
	buffDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	buffDesc.MiscFlags = 0;
	buffDesc.StructureByteStride = 0;

	//light::prepare(&m_PSData);
	verData.pSysMem = &prData->m_PSDataObject;

	res = gDevice->dxDevice->CreateBuffer(&buffDesc, &verData, &prData->PSObjectConstBuffer);


	//vertex buffer
	createObjects();
	vertexBufferSize = vertex.size();


	buffDesc.ByteWidth = sizeof(VertexPositionNormalTextureTangent) * vertexBufferSize;
	buffDesc.Usage = D3D11_USAGE_IMMUTABLE;
	buffDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	buffDesc.MiscFlags = 0;
	buffDesc.StructureByteStride = 0;

	verData.pSysMem = vertex.data();

	res = gDevice->dxDevice->CreateBuffer(&buffDesc, &verData, &vertexBuffer);

	//stan
	gDevice->dxContext->VSSetConstantBuffers(0, 1, &prData->VSConstBuffer);
	gDevice->dxContext->PSSetConstantBuffers(0, 1, &prData->PSFrameConstBuffer);
	gDevice->dxContext->PSSetConstantBuffers(1, 1, &prData->PSObjectConstBuffer);
	gDevice->dxContext->PSSetSamplers(0, 1, &m_sampleState);
	gDevice->dxContext->PSSetShaderResources(0, 1, &SResViewTexPlazma);
	
	gDevice->dxContext->IASetInputLayout(inLayout);
	
	/*gDevice->dxContext->VSSetShader(m_vshader, 0, 0);  
	gDevice->dxContext->PSSetShader(m_pshader, 0, 0);*/
   	UINT stride = sizeof(VertexPositionNormalTextureTangent);
	UINT offset = 0;
	gDevice->dxContext->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
	gDevice->dxContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	//przezroczystosc
	D3D11_BLEND_DESC transparentDesc = { 0 };
	transparentDesc.AlphaToCoverageEnable = false;
	transparentDesc.IndependentBlendEnable = false;
	transparentDesc.RenderTarget[0].BlendEnable = true;
	transparentDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	transparentDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	transparentDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	transparentDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	transparentDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	transparentDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	transparentDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

	
	gDevice->dxDevice->CreateBlendState(&transparentDesc, &transparentBlendState);
	float blendFactors[] = { 0.0f, 0.0f, 0.0f, 0.0f };
	gDevice->dxContext->OMSetBlendState(transparentBlendState, blendFactors, 0xffffffff);





	//smieci
	
	prData->projMatrix = Matrix::CreatePerspectiveFieldOfView(0.25f*3.14f, gDevice->aspectRatio(), 0.1f,30.0f);
	//projMatrix = Matrix::CreateOrthographic(12.80, 7.20, 0.1f, 30.0f);
	
	prData->m_PSDataFrame.gPointLight.Position = Vector3(0.0f, 1.0f, 5.0f);
	animation = 0;

	prData->m_PSDataFrame.gDirLight.Ambient = Vector4(0.9f, 0.9f, 0.9f, 1.0f);
	prData->m_PSDataFrame.gDirLight.Diffuse = Vector4(0.7f, 0.7f, 0.7f, 1.0f);
	prData->m_PSDataFrame.gDirLight.Specular = Vector4(0.9f, 0.9f, 0.9f, 1.0f);
	Vector3 temp = Vector3(-1, -3, 4);
	temp.Normalize();
	prData->m_PSDataFrame.gDirLight.Direction = temp;
	
	prData->m_PSDataFrame.gPointLight.Position = Vector3(1.0f, 13.1f,15);
	prData->m_PSDataFrame.gPointLight.Ambient = Vector4(0.3f, 0.3f, 0.3f, 1.0f);
	prData->m_PSDataFrame.gPointLight.Diffuse = Vector4(0.7f, 0.7f, 0.7f, 1.0f);
	prData->m_PSDataFrame.gPointLight.Specular = Vector4(0.9f, 0.9f, 0.9f, 1.0f);
	prData->m_PSDataFrame.gPointLight.Att = Vector3(1.0f, 0.0f, 0);
	prData->m_PSDataFrame.gPointLight.range = 40.0f;
	
}

void control::loop()
{
	keyboardInput();
	if (objectsToUpdate)
	{
		updateObjects();
		objectsToUpdate = false;
	}
	prData->deltaTime = mTimeControl.frame();
	prData->viewMatrix = m_camera.computeCamera();	
	prData->viewProjMatrix = prData->viewMatrix* prData->projMatrix;
	prData->m_PSDataFrame.gEyePosW = Vector3::Transform(Vector3::Zero, prData->viewMatrix.Invert());

	animation = animation + 5*prData->deltaTime;
	animation = fmod(animation,20*3.14);
	//m_PSData.gPointLight.Position = Vector3::Transform(m_PSData.gPointLight.Position, Matrix::CreateRotationY(1.5f*deltaTime));

	//setDefaultState();
	//gDevice->dxContext->PSSetShaderResources(0, 1, &SResViewTexPlazma);
	gDevice->dxContext->UpdateSubresource(prData->PSFrameConstBuffer, 0, NULL, &prData->m_PSDataFrame, 0, 0);


	gDevice->dxContext->ClearRenderTargetView(gDevice->renderView, DirectX::Colors::Blue );
	gDevice->dxContext->ClearDepthStencilView(gDevice->depthStencilView, D3D11_CLEAR_DEPTH, 1, 0);
	
	
	
	teren->draw();


	//temp
	/*m_PSData.gLighting.Ambient = Vector4(ambient, ambient, ambient, 1.0f);
	m_PSData.gLighting.Diffuse = Vector4(Diffuse, Diffuse, Diffuse, 1.0f);
	m_PSData.gLighting.Specular = Vector4(Specular, Specular, Specular, Sphardness);


	m_PSData.gDirLight.Ambient = Vector4(0.3f, 0.3f,0.3f,0.0f);
	m_PSData.gDirLight.Diffuse = Vector4(0.5f, 0.5f, 0.5f, 1.0f);
	m_PSData.gDirLight.Specular = Vector4(0.5f, 0.5f, 0.5f, 1.0f);*/
	/*Vector3 temp = Vector3(-1, -4, -1);
	temp.Normalize();
	prData->m_PSDataFrame.gDirLight.Direction = temp;*/

	/*m_PSData.gPointLight.Position = Vector3(0.0f, 1.1f, -4);
	m_PSData.gPointLight.Ambient = Vector4(0.8f, 0.8f, 0.8f, 1.0f);
	m_PSData.gPointLight.Diffuse = Vector4(0.5f, 0.5f, 0.5f, 1.0f);
	m_PSData.gPointLight.Specular = Vector4(0.5f, 0.5f, 0.5f, 1.0f);
	m_PSData.gPointLight.Att = Vector3(0.2f, 0.1f, 0);
	m_PSData.gPointLight.range = 200.0f;*/

	//gDevice->dxContext->UpdateSubresource(PSConstBuffer, 0, NULL, &m_PSData, 0, 0);
	//water->draw();
	//setDefaultState();
		
		//kula->worldMatrix = Matrix::CreateTranslation(sin(animation),0,0);
		//kula->worldMatrix = Matrix::CreateRotationY(3*animation) *kula->worldMatrix;
	//kula->worldMatrix = Matrix::CreateTranslation(0, 0, 0);
	//gDevice->dxContext->PSSetShaderResources(0, 1, &SResViewParticle);	
	woda3->draw();
	woda_gorna->m_lighting.Ambient = Vector4(ambient, ambient, ambient, 1.0f);
	woda_gorna->m_lighting.Diffuse = Vector4(Diffuse, Diffuse, Diffuse, 1.0f);
	woda_gorna->m_lighting.Specular = Vector4(Specular, Specular, Specular, Sphardness);
	
	gDevice->readonlyDepth(); //powoduje problemy przy patrzeniu z tylu
	
	woda_gorna->draw();
	
	

	

	m_particlesystem->Draw();
	
	setDefaultState();

	//2d tekst
	//gDevice->d2dRenderTarget->BeginDraw();
	//gDevice->d2dRenderTarget->DrawTextW(to_wstring(mTimeControl.fps).c_str(), to_wstring(mTimeControl.fps).length(), gDevice->wrFormat, D2D1::RectF(0, 0, 512, 512), gDevice->d2dSolidBrush);
	
	//gDevice->d2dRenderTarget->EndDraw();
	gDevice->present();
}

void control::setDefaultState()
{
	float blendFactors[] = { 0.0f, 0.0f, 0.0f, 0.0f };
	gDevice->dxContext->OMSetBlendState(transparentBlendState, blendFactors, 0xffffffff);
	gDevice->enableDepth();

	UINT stride = sizeof(VertexPositionNormalTextureTangent);
	UINT offset = 0;
	gDevice->dxContext->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
	gDevice->dxContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	gDevice->dxContext->IASetInputLayout(inLayout);

	
	gDevice->dxContext->PSSetSamplers(0, 1, &m_sampleState);
	gDevice->dxContext->PSSetShaderResources(0, 1, &SResViewTexPlazma);
	//gDevice->dxContext->PSSetShader(m_pshader, 0, 0);
	gDevice->dxContext->GSSetShader(NULL, 0, 0);

	gDevice->dxContext->VSSetConstantBuffers(0, 1, &prData->VSConstBuffer);
	gDevice->dxContext->PSSetConstantBuffers(0, 1, &prData->PSFrameConstBuffer);
	gDevice->dxContext->PSSetConstantBuffers(1, 1, &prData->PSObjectConstBuffer);

	//gDevice->dxContext->VSSetShader(m_vshader, 0, 0);
	
}

mesh * control::addMesh(std::vector <VertexPositionNormalTextureTangent> meshVertex, meshMaterial* mat)
{
	
	int size = meshVertex.size();
	int start = vertex.size();
	vertex.insert(vertex.end(), meshVertex.begin(), meshVertex.end());
	return new mesh(start,size,prData,mat);
}

void control::createObjects()
{
	/*vector<VertexPositionNormalTextureTangent> temp;

	temp.push_back(VertexPositionNormalTextureTangent(XMFLOAT3(-15.0f, -1.5f, -15.0f), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(0, 0), XMFLOAT3(0.0f, 0.0f, 1.0f)));
	temp.push_back(VertexPositionNormalTextureTangent(XMFLOAT3(15.0f, -1.5f, -15.0f), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(5, 0), XMFLOAT3(0.0f, 0.0f, 1.0f)));
	temp.push_back(VertexPositionNormalTextureTangent(XMFLOAT3(-15.0f, -1.5f, 15.0f), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(0, 5), XMFLOAT3(0.0f, 0.0f, 1.0f)));
	temp.push_back(VertexPositionNormalTextureTangent(XMFLOAT3(-15.0f, -1.5f, 15.0f), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(0, 5), XMFLOAT3(0.0f, 0.0f, 1.0f)));
	temp.push_back(VertexPositionNormalTextureTangent(XMFLOAT3(15.0f, -1.5f, -15.0f), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(5, 0), XMFLOAT3(0.0f, 0.0f, 1.0f)));
	temp.push_back(VertexPositionNormalTextureTangent(XMFLOAT3(15.0f, -1.5f, 15.0f), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(5, 5), XMFLOAT3(0.0f, 0.0f, 1.0f)));
	//kula = addMesh(shapes::sphereVPNTT(3));
	//kula = addMesh(meshLoader::loadVPNTT("obiekt1.obj"), defMaterial);*/


	//plain* pl1 = new plain(Vector2(0.1f, 0.1f), 10, 85,16);
	////pl1->wave();
	//
	//
	//woda1 = addMesh(shapes::plainVPNTT(*pl1), water);
	//woda1->worldMatrix = Matrix::CreateTranslation(-0.49f, 1.5f, -8.5f);
	//woda1->m_lighting.Ambient = Vector4(ambient, ambient, ambient, 1.0f);
	//woda1->m_lighting.Diffuse = Vector4(Diffuse, Diffuse, Diffuse, 1.0f);
	//woda1->m_lighting.Specular = Vector4(Specular, Specular, Specular, Sphardness);
	//piana_decal = addMesh(shapes::rectangleVPNTT(Vector2(0.5f, 0.5f)), defMaterial);
	//piana_decal->worldMatrix = Matrix::CreateRotationX(-3.14 / 2);
	//piana_decal->worldMatrix = piana_decal->worldMatrix *  Matrix::CreateTranslation(-0.3f, -0.09f, 2.5f);


	//teren = addMesh(meshLoader::loadVPNTT("teren_wodospad.obj"), defMaterial);
	//teren->worldMatrix = Matrix::CreateScale(width_mul, height_mul, 1.0f); //edycja
	
	


	teren = addMesh(mTerrain.generateVertex(), defMaterial);
	prData->collisionPlane = mTerrain.getCollisionPlane();
	teren->m_lighting.Specular = Vector4(0.05f, 0.05f, 0.05f, 250.0f);

	
	

	plain* pl2 = new plain(Vector2((width_mul*0.1f) + 0.06f, 0.85f), 10, 11);
	
	woda3 = addMesh(shapes::plainVPNTT(*pl2),water);
	delete pl2;
	woda3->worldMatrix = Matrix::CreateTranslation(width_mul*-0.5f - 0.3f, -0.1f, -0.85f);
	woda3->m_lighting.Ambient = Vector4(ambient, ambient, ambient, 1.0f);
	woda3->m_lighting.Diffuse = Vector4(Diffuse, Diffuse, Diffuse, 1.0f);
	woda3->m_lighting.Specular = Vector4(Specular, Specular, Specular, Sphardness);


	Plane temp_plane = Plane::Transform(prData->collisionPlane, Matrix::CreateTranslation(0, -height_mul*1.5f, -mTerrain.cliff_pos));
	woda_gorna = addMesh(shapes::upperWaterModel(height_mul*1.5f,8.5f, width_mul-0.1f, 0.2f,1.0f, temp_plane), water);
	woda_gorna->worldMatrix = Matrix::CreateTranslation(0, height_mul*1.5f, mTerrain.cliff_pos);
	woda_gorna->m_lighting.Ambient = Vector4(ambient, ambient, ambient, 1.0f);
	woda_gorna->m_lighting.Diffuse = Vector4(Diffuse, Diffuse, Diffuse, 1.0f);
	woda_gorna->m_lighting.Specular = Vector4(Specular, Specular, Specular, Sphardness);

}

void control::createParticleSystems()
{

	vector<Vector3> emitersPos;
	
	emitersPos.push_back(Vector3(-0.45f, 1.25f, 0.05));
	emitersPos.push_back(Vector3(-0.30f, 1.25f, 0.05));
	emitersPos.push_back(Vector3(-0.15, 1.25f, 0.05));
	emitersPos.push_back(Vector3(0, 1.25f, 0.05));
	emitersPos.push_back(Vector3(0.15, 	1.25f, 0.05));
	emitersPos.push_back(Vector3(0.30f, 1.25f, 0.05));
	emitersPos.push_back(Vector3(0.45f, 1.25f, 0.05));
	
	m_particlesystem = new particleSystem(prData, 5500,emitersPos);  
	/*m_particlesystem2 = new particleSystem(prData, 1000);
	m_particlesystem3 = new particleSystem(prData, 1000);
	m_particlesystem4 = new particleSystem(prData, 1000);
	m_particlesystem5 = new particleSystem(prData, 1000);*/


	ID3D11ShaderResourceView* SResViewTexWaterParticle;
	CreateDDSTextureFromFile(gDevice->dxDevice, L"water_particle.dds", nullptr, &SResViewTexWaterParticle);
	//CreateWICTextureFromFile(gDevice->dxDevice, L"water_particle.png", nullptr, &SResViewTexWaterParticle);



	
	m_particlesystem->mTexArray = SResViewTexWaterParticle;
	
}

void control::keyboardInput()
{
	kTracker.Update(prData->m_keyboard->GetState());

	if(kTracker.pressed.Q)
	{
		ambient += 0.1f;
	}
	if (kTracker.pressed.A)
	{
		ambient -= 0.1f;
	}
	if (kTracker.pressed.W)
	{
		Diffuse += 0.1f;
	}
	if (kTracker.pressed.S)
	{
		Diffuse -= 0.1f;
	}
	if (kTracker.pressed.E)
	{
		Specular += 0.1f;
	}
	if (kTracker.pressed.D)
	{
		Specular -= 0.1f;
	}
	if (kTracker.pressed.R)
	{
		Sphardness += 0.1f;
	}
	if (kTracker.pressed.F)
	{
		Sphardness -= 0.1f;
	}
	if (kTracker.pressed.Up)
	{
		height_mul += 0.1f;
		mTerrain.changeHeight(0.15);
		objectsToUpdate = true;
	}
	if (kTracker.pressed.Down)
	{
		height_mul -= 0.1f;
		mTerrain.changeHeight(-0.15);
		objectsToUpdate = true;
	}
	if (kTracker.pressed.Right)
	{
		width_mul += 0.1f;
		mTerrain.changeWidth(0.1);
		objectsToUpdate = true;
	}
	if (kTracker.pressed.Left)
	{
		width_mul -= 0.1f;
		mTerrain.changeWidth(-0.1);
		objectsToUpdate = true;
	}
	if (kTracker.pressed.Z)
	{
		mTerrain.moveCliff( 0.1f);
		objectsToUpdate = true;
	}
	if (kTracker.pressed.X)
	{
		mTerrain.moveCliff(-0.1f);
		objectsToUpdate = true;
	}
	
}

void control::updateObjects()
{
	D3D11_BUFFER_DESC buffDesc;
	D3D11_SUBRESOURCE_DATA verData;

	delete woda_gorna;
	delete	woda3; //wszystko do poprawienia bardzo temp
	delete	teren;
	vertex.clear();
	vertexBuffer->Release();

	createObjects();
	vertexBufferSize = vertex.size();

	buffDesc.ByteWidth = sizeof(VertexPositionNormalTextureTangent) * vertexBufferSize;
	buffDesc.Usage = D3D11_USAGE_IMMUTABLE;
	buffDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	buffDesc.MiscFlags = 0;
	buffDesc.StructureByteStride = 0;

	verData.pSysMem = vertex.data();

	gDevice->dxDevice->CreateBuffer(&buffDesc, &verData, &vertexBuffer);


	// wszystko od tego do poprawienia wzory nie dzialaja poprawnie
	int emiters_number = 1 + (width_mul*1.0f) / 0.2f; //0.35 odleglosc normalna pomidzy emiterami
	float emiter_split = width_mul / emiters_number;
	vector<Vector3> emitersPos;
	for (int i = 0; i <= emiters_number; i++)
	{
		emitersPos.push_back(Vector3((-0.5f*width_mul) + 0.05f +(i*emiter_split), height_mul*1.5f - 0.25f, mTerrain.cliff_pos+ 0.05f));
	}

	

	m_particlesystem->Update(emitersPos);
}











