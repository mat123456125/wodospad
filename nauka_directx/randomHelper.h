#pragma once

#include <random>
#include <math.h>
#include <SimpleMath.h>

class randomHelper
{
public:
	randomHelper();
	~randomHelper();
	float randf();
	float randf(float a, float b);
	DirectX::SimpleMath::Vector3 randUnitVector3();
private:
	std::mt19937 generator;
};

